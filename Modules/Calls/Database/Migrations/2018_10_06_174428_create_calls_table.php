<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['first_contact','validate_information','other_subject'])->default('first_contact');
            $table->enum('status',['success','fail'])->default('fail');
            $table->string('description')->nullable();
            $table->string('duration',5);
            $table->timestamp('date');

            $table->morphs('callable');

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
