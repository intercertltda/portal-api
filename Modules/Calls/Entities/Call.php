<?php

namespace Modules\Calls\Entities;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'description', 'duration', 'date'
    ];

    protected $dates = [ 'date' ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'type', 'status', 'agent_id', 'client_id', 'collaborator_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function callable()
    {
        return $this->morphTo();
    }
}
