<?php

return [
    'name' => 'InterSIG - Gestão de Processos',


    'menu' => [
        'dashboard' => [
            'url' => '/',
            'route' => 'dashboard.index',
            'icon' => 'mdi mdi-view-dashboard',
            'text' => __('Dashboard'),
            'type' => 'link',

            'check' => 'permission',
            'param' => ['panel']
        ],

        'steps' => [
            'route' => 'steps.index',
            'icon' => 'fa fa-chart-bar',
            'text' => __('Processes management'),
            'type' => 'items',

            'check' => 'role',
            'param' => [ 'admin', 'directors' ],

            'items' => [
                'steps.index' => [
                    'route' => 'steps.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Steps'),
                    'type' => 'link',

                    'check' => 'role',
                    'param' => [ 'admin', 'directors' ]
                ],
                'process.index' => [
                    'route' => 'steps.processes.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Processes'),
                    'type' => 'link',

                    'check' => 'role',
                    'param' => [ 'admin', 'directors' ]
                ],
                'manager.order' => [
                    'route' => 'manager.order',
                    'icon' => 'fas fa-sort',
                    'text' => __('Ordering'),
                    'type' => 'link',

                    'check' => 'role',
                    'param' => [ 'admin', 'directors' ]
                ]
            ]
        ],
        'viability' => [
            'route' => 'viability.index',
            'icon' => 'fa fa-chart-bar',
            'text' => __('Viability'),
            'type' => 'items',

            'check' => 'role',
            'param' => [ 'directors', 'admin' ],

            'items' => [
                'viability.index' => [
                    'route' => 'viability.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Listing'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ],

                'viability.create' => [
                    'route' => 'viability.create',
                    'icon' => 'fas fa-plus',
                    'text' => __('Create'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ],

                'viability.responses' => [
                    'route' => 'viability.responses',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Responses'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ]
            ]
        ],
        'companies' => [
            'route' => 'companies.index',
            'icon' => 'fa fa-building',
            'text' => __('Companies'),
            'type' => 'items',

            'check' => 'role',
            'param' => [ 'admin' ],

            'items' => [
                'companies.index' => [
                    'route' => 'companies.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Listing'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ],

                'companies.create' => [
                    'route' => 'companies.create',
                    'icon' => 'fas fa-plus',
                    'text' => __('Create'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ]
            ]
        ],
        'companies.my' => [
            'route' => 'companies.my.index',
            'icon' => 'fa fa-building',
            'text' => __('My Companies'),
            'type' => 'link',

            'check' => 'role',
            'param' => [ 'directors' ],
        ],
        'departments' => [
            'route' => 'departments.index',
            'icon' => 'fa fa-building',
            'text' => __('Departments'),
            'type' => 'items',

            'check' => 'role',
            'param' => [ 'directors', 'admin' ],

            'items' => [
                'departments.index' => [
                    'route' => 'departments.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Listing'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ],

                'departments.create' => [
                    'route' => 'departments.create',
                    'icon' => 'fas fa-plus',
                    'text' => __('Create'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ]
            ]
        ],
        'branches' => [
            'route' => 'branches.index',
            'icon' => 'fa fa-users',
            'text' => __('Branches'),
            'type' => 'link',

            'check' => 'role',
            'param' => [ 'directors', 'admin' ],
        ],
        'leads' => [
            'route' => 'leads.index',
            'icon' => 'fa fa-address-book',
            'text' => __('Leads'),
            'type' => 'items',
            'check' => 'role',
            'param' => [ 'admin', 'marketing', 'commercial' ],
            'items' => [
                'leads.index' => [
                    'route' => 'leads.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Listing'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ],
                'leads.create' => [
                    'route' => 'leads.create',
                    'icon' => 'fas fa-plus',
                    'text' => __('Create'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ]
            ]
        ],
        'users' => [
            'route' => 'users.index',
            'icon' => 'fa fa-users',
            'text' => __('Collaborators'),
            'type' => 'items',
            'check' => 'role',
            'param' => [ 'directors', 'admin' ],
            'items' => [
                'users.index' => [
                    'route' => 'users.index',
                    'icon' => 'fas fa-list-ul',
                    'text' => __('Listing'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'panel' ]
                ],

                'users.create' => [
                    'route' => 'users.create',
                    'icon' => 'fas fa-plus',
                    'text' => __('Create'),
                    'type' => 'link',

                    'check' => 'permission',
                    'param' => [ 'admin' ]
                ]
            ]
        ],
    ]
];
