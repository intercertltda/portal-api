{{ Form::label($select['name'], $select['label']) }}
{{ Form::select($select['name-array'] ?? $select['name'], $select['list'], $select['old'], [
    'class' => 'form-control ' . $select['name'],
    'id' => $select['name'],
    'title' => $select['title'] ?? 'Selecione um item',
    'data-live-search' => 'true',
    'data-size' => 4,
    isset($select['multiple']) && $select['multiple'] ? 'multiple' : null,
    isset($select['required']) && $select['required'] ? 'required' : null,
    isset($select['disabled']) && $select['disabled'] ? 'disabled' : null,
]) }}


@push('styles')
    <link rel="stylesheet" href="{{ asset('components/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            $('select#{{ $select['name'] }}').selectpicker();
        })(jQuery);
    </script>
@endpush