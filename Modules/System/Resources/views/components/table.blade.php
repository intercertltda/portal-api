<div class="table-responsive">
    <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
                @foreach($thead as $key => $th)
                    @php
                        if (str_contains($key, 'center')) {
                            $class = 'text-center';
                        } elseif (str_contains($key, 'right')) {
                            $class = 'text-right';
                        } else {
                            $class = 'text-left';
                        }
                    @endphp
                    <th class="{{ $class }}">{{ $th }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            {{ $slot }}
        </tbody>
    </table>
</div>

@push('styles')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endpush


@push('scripts')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
@endpush
@push('inline-scripts')
    <script type="text/javascript">
        (($) => {
            $("#zero_config").DataTable();
        })(jQuery)
    </script>
@endpush