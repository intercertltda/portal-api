<!-- footer -->
<!-- ============================================================== -->
<footer class="footer main-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <p>@ {{ date('Y') }} | <a href="https://intercet.com.br">{{ strtoupper(config('developer.business.name')) }}</a></p>
            </div>
            <div class="col-xs-12 col-md-6 text-right">
                <ul>
                    <li><a href="{{ config('developer.url.support') }}">{{ __('Support') }}</a></li>
                    <li><a href="{{ config('developer.url.contact') }}">{{ __('Contact') }}</a></li>
                    <li><a href="{{ config('developer.url.faq') }}">{{ __('FAQ') }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->

