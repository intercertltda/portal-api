<!-- jQuery 3 -->
<script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('components/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
    window.paceOptions = {
        ajax: {
            trackMethods: ['GET', 'POST', 'PUT', 'DELETE', 'REMOVE']
        }
    };
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'X-USER-TOKEN': $('meta[name="user-reference"]').attr('content'),
            'X-Requested-With': 'XMLHttpRequest',
        }
    });
</script>
<script src="{{ asset('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{ asset('assets/extra-libs/sparkline/sparkline.js')}}"></script>
<script src="{{ asset('assets/js/waves.js')}}"></script>
<script src="{{ asset('assets/js/sidebarmenu.js')}}"></script>
<script src="{{ asset('assets/js/custom.min.js')}}"></script>
<script src="{{ asset('assets/libs/toastr/build/toastr.min.js')}}"></script>
<!--This page JavaScript -->
<!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
<!-- Charts js Files -->
<script src="{{ asset('assets/libs/flot/excanvas.js')}}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.js')}}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.pie.js')}}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.time.js')}}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.stack.js')}}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
<script src="{{ asset('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{ asset('assets/js/pages/chart/chart-page-init.js')}}"></script>
<script src="{{ asset('js/system.min.js')}}"></script>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    @if(session('status'))
        toastr['{{session("status")}}']("{{session('msg')}}");
    @endif



</script>
