<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">
                {{ $title or '' }}
                @isset($subtitle)
                    <small>{{ $subtitle }}</small>
                @endisset

                <span class="push-buttons" style="margin-left: 15px;">
                    @stack('push-buttons')
                </span>
            </h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        @foreach($breadcrumb as $key => $item)
                            @if($item['current'])
                                <li class="breadcrumb-item active" aria-current="page">@isset($item['icon'])<i class="{{ $item['icon'] }}"></i>@endisset {{ $item['title'] }}</li>
                            @else
                                <li class="breadcrumb-item">
                                    <a href="{{ $item['url'] }}">
                                        @isset($item['icon'])<i class="{{ $item['icon'] }}"></i>@endisset {{ $item['title'] }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if($errors->any() or session()->has('fail') or session()->has('success'))
    <div class="container-fluid" style="min-height: auto; padding-bottom: 0;">
        @if ($errors->any())
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" style="margin-bottom: 0;">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        @if(session()->has('fail'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" style="margin-bottom: 0;">
                        <span>{{ session()->get('fail') }}</span>
                    </div>
                </div>
            </div>
        @endif
        @if(session()->has('success'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success" style="margin-bottom: 0;">
                        <span>{{ session()->get('success') }}</span>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endif