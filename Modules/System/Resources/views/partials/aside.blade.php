<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                @foreach(config('system.menu') as $key => $item)
                    @if(is_check($item['param'], $item['check']))
                        @if($item['type'] == 'link')
                            <li class="sidebar-item {{ in_route($key) ? 'selected' : '' }}">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link {{ in_route($key) ? 'active' : '' }}" href="{{ get_route($item['route']) }}" aria-expanded="false">
                                    <i class="{{ $item['icon'] }}"></i><span class="hide-menu">{{ $item['text'] }}</span>
                                </a>
                            </li>
                        @elseif($item['type'] == 'items')
                            <li class="sidebar-item {{ in_route($key) ? 'selected' : '' }}">
                                <a class="sidebar-link has-arrow waves-effect waves-dark {{ in_route($key) ? 'active' : '' }}" href="javascript:void(0)" aria-expanded="false">
                                    <i class="{{ $item['icon'] }}"></i><span class="hide-menu">{{ $item['text'] }}</span>
                                </a>
                                <ul aria-expanded="false" class="collapse first-level  {{ in_route($key) ? 'in' : '' }}">
                                    @foreach($item['items'] as $subKey => $subItem)
                                        @if(is_check($subItem['param'], $subItem['check']))
                                            <li class="sidebar-item">
                                                <a href="{{ get_route($subItem['route']) }}" class="sidebar-link">
                                                    <i class="{{ $subItem['icon'] }}"></i><span class="hide-menu">{{ $subItem['text'] }}</span>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endif
                @endforeach
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->