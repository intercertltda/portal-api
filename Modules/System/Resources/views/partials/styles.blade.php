<!-- Custom CSS -->
<link href="{{ asset('assets/libs/flot/css/float-chart.css')}}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ asset('assets/css/style.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">
<link href="{{ asset('css/app.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/libs/snackbar/css/style.css')}}" rel="stylesheet">
<!-- Toastr CSS -->
<link href="{{ asset('assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
