<?php

namespace Modules\System\Facades;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaginateFacade
{
    /**
     * @var int
     */
    private $current;

    /**
     * @var int
     */
    private $total;

    /**
     * @var int
     */
    private $limit;
    /**
     * @var Request
     */
    private $request;

    /**
     * Paginate constructor.
     * @param Request $request
     * @param int $current
     * @param int $total
     * @param int $limit
     */
    public function __construct(Request $request, int $current, int $total, int $limit = 2)
    {
        $this->current = $current;
        $this->total = $total;
        $this->limit = $limit;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function make()
    {
        $ranges = [[1, 1 + $this->limit]];
        self::mergeRanges($ranges, ($this->current - $this->limit), ($this->current + $this->limit));
        self::mergeRanges($ranges, ($this->total - $this->limit), $this->total);

        // initialise the list of links
        $links = [];

        // loop over the ranges
        foreach ($ranges as $range){

            // if there are preceeding links, append the ellipsis
            if (count($links) > 0) $links[] = '<li class="page-item disabled"><span class="page-link">&hellip;</span></li>';

            // merge in the new links
            $links = array_merge($links, self::links($range, $this->current));

        }

        // return the links
        return implode(' ' , $links);
    }

    /**
     * Merges a new range into a list of ranges, combining neighbouring ranges.
     * The parameters are:
     *
     * @param $ranges - the list of ranges
     * @param $start  - the start of the new range
     * @param $end    - the end of the new range
     */
    static private function mergeRanges(&$ranges, $start, $end)
    {
        // determine the end of the previous range
        $endOfPreviousRange =& $ranges[count($ranges) - 1][1];

        // extend the previous range or add a new range as necessary
        if ($start <= $endOfPreviousRange + 1):
            $endOfPreviousRange = $end;
        else:
            $ranges[] = [$start, $end];
        endif;
    }

    /**
     * Create the links for a range. The parameters are:
     * @param $range      - the range
     * @param $page       - the current page
     * @param $linkFormat - the format for links
     * @param $pageFormat - the format for the current page
     * @return array
     */
    private function links($range, $page)
    {
        $linkFormat = '<li class="page-item"><a class="page-link" href="%s">%d</a></li>';
        $pageFormat = '<li class="page-item active"><span class="page-link" ref="%s">%d</span></li>';

        // initialise the list of links
        $links = [];

        // loop over the pages, adding their links to the list of links
        for ($index = $range[0]; $index <= $range[1]; $index ++){
            if ($index < 2) {
                $url = url_replace('page', null, request()->url());
            } else {
                $url = $this->request->fullUrlWithQuery(['page' => $index]);
            }

            $links[] = sprintf(($index === $page ? $pageFormat : $linkFormat), $url, $index);
        }

        // return the array of links
        return $links;
    }
}
