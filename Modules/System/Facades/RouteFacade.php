<?php

namespace Modules\System\Facades;


use Illuminate\Support\Facades\Route;

class RouteFacade
{
    /**
     * @param array $params
     * @param string $controller
     * @param string $ref
     * @return void
     */
    static public function group(array $params, string $controller, string $ref = 'reference')
    {
        Route::group($params, function() use ($params, $controller, $ref) {

            if (isset($ref))
                $reference = sprintf('{%s}', $ref);


            Route::get('', sprintf('%s@index', $controller))->name('index');
            Route::get('criar', sprintf('%s@create', $controller))->name('create');

            Route::get(sprintf('%s/editar', $reference), sprintf('%s@edit', $controller))->name('edit');
            Route::get(sprintf('%s', $reference), sprintf('%s@show', $controller))->name('show');


            Route::post('', sprintf('%s@store', $controller))->name('store');
            Route::post(sprintf('%s', $reference), sprintf('%s@update', $controller))->name('update');
            Route::delete(sprintf('%s', $reference), sprintf('%s@destroy', $controller))->name('delete');
        });
    }

    /**
     * @param array $params
     * @param string $controller
     * @param string $ref
     * @return void
     */
    static public function api(array $params, string $controller, string $ref = 'reference')
    {
        Route::group($params, function() use ($params, $controller, $ref) {

            if (isset($ref))
                $reference = sprintf('{%s}', $ref);


            Route::get('', sprintf('%s@index', $controller))->name('index');
            Route::get(sprintf('%s', $reference), sprintf('%s@show', $controller))->name('show');


            Route::post('', sprintf('%s@store', $controller))->name('store');
            Route::post(sprintf('%s', $reference), sprintf('%s@update', $controller))->name('update');
            Route::delete(sprintf('%s', $reference), sprintf('%s@destroy', $controller))->name('delete');
        });
    }
}
