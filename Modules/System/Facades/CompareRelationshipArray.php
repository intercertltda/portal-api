<?php

namespace Modules\System\Facades;


class CompareRelationshipArray
{
    /**
     * @param array $old
     * @param array $new
     * @return array
     */
    static public function check(array $old, array $new)
    {
        foreach($old as $item)
        {
            if (in_array($item, $new)) {
                $continue[] = $item;
            } else {
                $removal[] = $item;
            }
        }

        foreach ($new as $item)
        {
            if (!in_array($item, $old))
                $news[] = $item;
        }

        return [
            'continue' => $continue ?? [],
            'removal' => $removal ?? [],
            'news' => $news ?? [],
        ];
    }
}
