<?php

namespace Modules\System\Facades;

use Illuminate\Support\Collection;

use Modules\System\Facades\PaginationFacade as Pagination;

class CollectionFacade
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * Collection constructor.
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return Pagination
     */
    public function pagination(int $perPage = 10) : Pagination
    {
        return (new Pagination($perPage, $this->collection));
    }

    /**
     * @return CollectionFacade
     */
    public function all() : CollectionFacade
    {
        return (new self($this->collection));
    }

    /**
     * @param $column
     * @param $needle
     * @return CollectionFacade
     */
    public function where($column, $needle) : CollectionFacade
    {
        if (str_contains($column, '.')) {
            $this->collection = $this->collection
                ->filter(function ($row) use ($column, $needle) {
                    $ex = explode('.', $column);

                    foreach ($row->{$ex[0]} as $a => $b)
                    {
                        if (is_collect($b)) {
                            foreach($b as $c => $d)
                            {
                                if ($c === $ex[1] && is_string_int($d) && str_search($d, $needle)) return true;
                            }
                        } else {
                            if (is_string_int($b) && str_search($b, $needle)) return true;
                        }
                    }
                });
        } else {
            $this->collection = $this->collection
                ->where($column, $needle);
        }

        return (new self($this->collection));
    }

    /**
     * @param $needle
     * @param null $inColumn
     * @return CollectionFacade
     */
    public function search($needle, $inColumn = null)
    {
        $this->collection = $this->collection
            ->filter(function ($item) use ($needle, $inColumn) {
                $arr = toArray($item);
                foreach ($arr as $a => $b)
                {
                    $get = isset($inColumn) ? $arr[$inColumn] : $b;

                    if (is_string_int($get)) {
                        if (str_search($get, $needle)) return true;
                    } elseif (is_collect($get)) {
                        foreach ($get as $c => $d)
                        {
                            if (is_string_int($d)) {
                                if (str_search($d, $needle)) return true;
                            } elseif (is_collect($d)) {
                                foreach ($d as $e => $f)
                                {
                                    if (is_string_int($f) && str_search($f, $needle)) return true;
                                }
                            }
                        }
                    }
                }
            });

        return (new self($this->collection));
    }


    /**
     * @return array
     */
    public function toArray()
    {
        return toArray($this->collection->toArray());
    }
}
