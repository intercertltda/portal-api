<?php

namespace Modules\System\Facades;


class DocumentFacade
{
    /**
     * @var string
     */
    private $document;

    /**
     * Create a new facade instance.
     *
     * @return void
     */
    public function __construct(string $document)
    {
        $this->document = $document;
    }

    /**
     * @return null|string
     */
    public function mask()
    {
        $type = $this->type();

        if ($type == 'undefined')
            return null;

        if($type === 'cpf' && $this->valid()):
            $mask = '%s%s%s.%s%s%s.%s%s%s-%s%s';
        elseif ($type === 'cnpj' && $this->valid()):
            // 99.999.999/9999-99
            $mask = '%s%s.%s%s%s.%s%s%s/%s%s%s%s-%s%s';
        endif;

        return helpers()->str_mask($this->document, $mask);
    }

    /**
     * @return string
     */
    public function type() : string
    {
        $clear = (string) preg_replace("/[^0-9]/", '', $this->document);

        if (strlen($clear) === 11)
            return 'cpf';

        if (strlen($clear) === 14)
            return 'cnpj';

        return 'undefined';
    }

    /**
     * @return bool
     */
    public function valid() : bool
    {
        $clear = (string) preg_replace("/[^0-9]/", '', $this->document);
        return strlen($clear) === 11 ? self::cpf($clear) : self::cnpj($clear);
    }

    /**
     * @param null $cpf
     * @return bool
     */
    private function cpf($cpf = null) : bool
    {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11)
            return false;
        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{10}/', $cpf))
            return false;

        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $cnpj
     * @return bool
     */
    private function cnpj($cnpj) : bool
    {
        // Valida tamanho
        if (strlen($cnpj) !== 14)
            return false;

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;

        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{13}/', $cnpj))
            return false;

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
}
