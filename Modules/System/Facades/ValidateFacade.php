<?php

namespace Modules\System\Facades;


class ValidateFacade
{
    static public function validate_mask($document)
    {
        $type = self::typeDocument($document);

        if ($type == 'undefined'):
            Log::info('Documento inválido');
            return null;
        endif;

        $valid = self::{$type}($document);

        if($type === 'cpf' && $valid):
            $mask = '%s%s%s.%s%s%s.%s%s%s-%s%s';
        elseif ($type == 'cnpj' && $valid):
            // 99.999.999/9999-99
            $mask = '%s%s.%s%s%s.%s%s%s/%s%s%s%s-%s%s';
        endif;

        return Str::str_mask($document, $mask);
    }

    /**
     * @param $document
     * @return string
     */
    static public function typeDocument($document)
    {
        $clear = (string) preg_replace("/[^0-9]/", '', $document);

        if (strlen($clear) === 11)
            return 'cpf';

        if (strlen($clear) === 14)
            return 'cnpj';

        return 'undefined';
    }

    /**
     * @param $document
     * @return bool
     */
    static public function document($document)
    {
        $clear = (string) preg_replace("/[^0-9]/", '', $document);
        return strlen($clear) === 11 ? self::cpf($clear) : self::cnpj($clear);
    }

    /**
     * @param null $cpf
     * @return bool
     */
    static public function cpf($cpf = null) {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11)
            return false;
        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{10}/', $cpf))
            return false;

        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $cnpj
     * @return bool
     */
    static public function cnpj($cnpj)
    {
        // Valida tamanho
        if (strlen($cnpj) !== 14)
            return false;

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;

        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{13}/', $cnpj))
            return false;

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
}
