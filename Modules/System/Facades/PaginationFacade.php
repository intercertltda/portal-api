<?php

namespace Modules\System\Facades;

use Illuminate\Support\Collection as SupportCollection;
use Modules\System\Facades\PaginateFacade;


class PaginationFacade
{
    /**
     * @var SupportCollection
     */
    private $collection;
    private $request;

    /**
     * @var int
     */
    private $perPage;

    /**
     * Collection constructor.
     * @param int $perPage
     * @param SupportCollection $collection
     */
    public function __construct(int $perPage, SupportCollection $collection)
    {
        $this->collection = $collection;
        $this->request = request();
        $this->perPage = $perPage;
    }

    /**
     * @return int
     */
    public function total() : int
    {
        return $this->collection->count();
    }

    /**
     * @return int
     */
    public function perPage() : int
    {
        return $this->perPage;
    }

    public function get()
    {
        return $this->collection->slice($this->offset(), $this->perPage);
    }

    /**
     * @return array|int|null|string
     */
    public function currentPage() : int
    {
        return floatval($this->request->input('page') ?? 1);
    }

    /**
     * @return int
     */
    public function totalPages() : int
    {
        $total = $this->collection->count();
        return (int) ceil($total / $this->perPage);
    }

    /**
     * @return int
     */
    public function offset() : int
    {
        $offset = $this->currentPage() === 1 ? 0 : ($this->currentPage() - 1) * $this->perPage;

        return ($offset < 0) ? 0 : $offset;
    }

    /**
     * @return array|int|null|string
     */
    public function nextPage() : int
    {
        return $this->currentPage() > 1
            ? $this->currentPage() + 1
            : 2;
    }

    /**
     * @return array|int|null|string
     */
    public function prevPage() : int
    {
        return $this->currentPage() >= 2
            ? $this->currentPage() - 1
            : 1;
    }

    /**
     * @return string
     */
    public function nextLink() : string
    {
        $page = $this->currentPage() === $this->totalPages() ? $this->totalPages() : $this->currentPage() + 1;

        return url_replace('page', ['page' => $page]);
    }

    /**
     * @return string
     */
    public function prevLink() : string
    {
        if ($this->currentPage() === 2):
            $uri = url_replace('page');
        else:
            $uri = url_replace('page', ['page' => $this->prevPage()]);
        endif;

        return $uri;
    }

    /**
     * @return PaginateFacade
     */
    public function pages() : PaginateFacade
    {
        return new PaginateFacade($this->request, $this->currentPage(), $this->totalPages());
    }
}
