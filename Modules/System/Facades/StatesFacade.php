<?php

namespace Modules\System\Facades;


use App\Classes\Collection;
use Illuminate\Support\Facades\File;

class StatesFacade
{
    /**
     * @var string
     */
    private $path;

    /**
     * Create a new facade instance.
     *
     * @return void
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function file() : string
    {
        return (string) File::get($this->path);
    }

    /**
     * @return mixed
     */
    public function decode()
    {
        return json_decode($this->file());
    }

    /**
     * @return array
     */
    public function get() : array
    {
        return $this->decode();
    }

    /**
     * @return array
     */
    public function list() : array
    {
        return Collection::setKey($this->get(), 'sigla', 'nome');
    }

    /**
     * @param string|null $uf
     * @return mixed
     */
    public function cities(string $uf = null, bool $jsonfy = false, bool $key = false)
    {
        foreach ($this->get() as $state)
        {
            foreach($state->cidades as $city)
            {
                if ($key) {
                    $list[$state->sigla][$city] = $city;
                } else {
                    $list[$state->sigla][] = $city;
                }
            }
        }

        $response = isset($uf) ? $list[$uf] : $list;

        return $jsonfy ? json_encode($response) : $response;
    }
}
