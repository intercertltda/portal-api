<?php

namespace Modules\System\Facades;


use Carbon\Carbon;

class HelpersFacade
{
    /**
     * @param string $document
     * @return DocumentFacade
     */
    static public function document(string $document, bool $clean = false)
    {
        if ($clean) $document = (new self)->clean($document);

        return new DocumentFacade($document);
    }

    /**
     * @param string $number
     * @return string
     */
    static public function phone(string $number)
    {
        if(strlen($number) === 10):
            $mask = '(%s%s) %s%s%s%s-%s%s%s%s';
        else:
            $mask = '(%s%s) %s %s%s%s%s-%s%s%s%s';
        endif;

        return (new static)->str_mask($number, $mask);
    }

    /**
     * @param string $string
     * @param bool $hyphens
     * @return mixed|null|string|string[]
     */
    static public function clean(string $string, $hyphens = false)
    {
        if ($hyphens) {
            $string = str_replace(' ', '-', $string);
        } else {
            $string = str_replace('-', '', $string);
        }
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        return $string;
    }

    /**
     * @param string $date
     * @param $format
     * @return string
     */
    static public function date(string $date, $format)
    {
        $carbon = Carbon::parse($date);

        return $carbon->format($format);
    }

    /**
     * @param $value
     * @return string
     */
    static public function firstName($value )
    {
        $value = strtolower($value);
        $value = explode(' ', $value);
        return ucfirst($value[0]);
    }

    /**
     * @param $name
     * @return bool|null|string|string[]
     */
    static public function nameToLogin($name) : string
    {
        $ex = explode(' ', $name);

        if (count($ex) > 1) {
            if (strlen($ex[1]) <= 3) {
                $name = sprintf('%s.%s', $ex[0], $ex[2]);
            } else {
                $name = sprintf('%s.%s', $ex[0], $ex[1]);
            }
        } else {
            $name = trim(self::clean($name));
        }

        return self::slugFy(strtolower($name), true);
    }

    /**
     * @param $text
     * @param bool $withDot
     * @return null|string|string[]
     */
    static public function slugFy(string $text, $withDot = false) : string
    {
        $chart = $withDot ? '.' : '-';

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', $chart, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text))
            return 'n-a';

        return $text;
    }

    /**
     * @param $string
     * @param $mask
     * @return string
     */
    static public function str_mask($string, $mask)
    {
        return vsprintf($mask, str_split($string));
    }
}
