<?php

namespace Modules\System\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CachingFacade
{
    /**
     * @return int
     */
    static public function time() : int
    {
        return config('developer.cache.list');
    }

    /**
     * @param string $name
     * @param array|null $select
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|mixed
     */
    static public function table(string $name, array $select = null, $custom = false)
    {
        $key = !$custom ? $name : now()->format('Y-m-d');

        if (Cache::has($key)):
            $response = Cache::get($key);
        else:
            $response = DB::table($name);
            if (isset($select))
                $response = $response->select($select);
            $response = $response->get();

            Cache::put($key, $response, self::time());
        endif;

        return collect($response);
    }

    /**
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|mixed
     */
    static public function users()
    {
        return self::table('users');
    }
}
