<?php

namespace Modules\Departments\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Departments\Entities\Department;
use Modules\Users\Entities\User;

class DepartmentsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $departments = [
            ['name' => 'Compliance', 'description' => 'Setor de Compliance'],
            ['name' => 'Financeiro', 'description' => 'Setor Financeiro da Empresa'],
        ];

        foreach ($departments as $department)
        {
            $model = new Department;
            $model->name = $department['name'];
            $model->description = $department['description'];
            $model->save();
            $user = User::where('login', 'felipe.rank')->first();
            $model->users()->attach($user);
        }
    }
}
