<?php

namespace Modules\Departments\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Users\Transformers\UsersRelationship;

class DepartmentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at->format('d/m/Y \à\s H:i'),
            'collaborators' => UsersRelationship::collection($this->users)
        ];
    }
}
