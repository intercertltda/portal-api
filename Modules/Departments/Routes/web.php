<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\System\Facades\RouteFacade;

RouteFacade::group([
    'prefix' => 'departamentos',
    'middleware' => ['auth', 'role:admin|directors'],
    'as' => 'departments.'
], 'DepartmentsController');