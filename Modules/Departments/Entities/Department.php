<?php

namespace Modules\Departments\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Companies\Entities\Company;
use Modules\Users\Entities\User;
use Spatie\Permission\Models\Role;

class Department extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    protected $table = 'departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    protected $appends = [ 'collaborators' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'departments_users', 'department_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * @return string
     */
    public function getCollaboratorsAttribute() : string
    {
        foreach($this->users()->get() as $user)
            $users[] = $user->name;

        return implode(', ', $users);
    }
}
