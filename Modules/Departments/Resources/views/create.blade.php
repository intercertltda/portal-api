@extends('system::layouts.master')
@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                          'url' => route('departments.store')
                        ]) }}
                            <div class="row form-group">
                                <div class="col-md-4">
                                    {{ Form::label('name', 'Nome') }}
                                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'required']) }}
                                </div>
                                <div class="col-md-4">
                                    @component('system::components.select', [
                                        'select' => [
                                            'name' => 'responsible',
                                            'name-array' => 'responsible[]',
                                            'title' => __('Select users leaders'),
                                            'label' => __('Leaders'),
                                            'list' => $collaborators,
                                            'old' => old('responsible'),
                                            'multiple' => true,
                                            'required' => true
                                        ]
                                    ])
                                    @endcomponent
                                </div>

                                <div class="col-md-4">
                                    @component('system::components.select', [
                                        'select' => [
                                            'name' => 'role',
                                            'title' => __('Select type of leaders'),
                                            'label' => __('Type of Leaders'),
                                            'list' => $permissions,
                                            'old' => old('role'),
                                            'required' => true
                                        ]
                                    ])
                                    @endcomponent
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    {{ Form::label('description', 'Descrição') }}
                                    {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description', 'rows' => 4]) }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    @component('system::components.select', [
                                        'select' => [
                                            'name' => 'company',
                                            'title' => __('Select company for this department'),
                                            'label' => __('Company'),
                                            'list' => $companies,
                                            'old' => old('company'),
                                            'required' => true
                                        ]
                                    ])
                                    @endcomponent
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    {{ Form::submit(__('Create'), ['class'=> 'btn btn-primary pull-right']) }}
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
