@extends('system::layouts.master')
@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                          'url' => route('departments.update', $item->reference)
                        ]) }}
                            {{ Form::hidden('__method', 'PUT') }}
                            <div class="row form-group">
                                <div class="col-md-4">
                                    {{ Form::label('name', 'Nome') }}
                                    {{ Form::text('name', $item->name ?? old('name'), ['class' => 'form-control', 'id' => 'name', 'required']) }}
                                </div>

                                <div class="col-md-4">

                                    @foreach($select as $i)
                                        {{ Form::hidden('old_responsible[]', $i) }}
                                    @endforeach

                                    @component('system::components.select', [
                                        'select' => [
                                            'name' => 'responsible',
                                            'name-array' => 'responsible[]',
                                            'title' => __('Select users leaders'),
                                            'label' => __('Leaders'),
                                            'list' => $collaborators,
                                            'old' => $select ?? old('responsible'),
                                            'multiple' => true,
                                            'required' => true
                                        ]
                                    ])
                                    @endcomponent
                                </div>

                                <div class="col-md-4">
                                    {{ Form::hidden('old_role', $item->role->name) }}
                                    @component('system::components.select', [
                                        'select' => [
                                            'name' => 'role',
                                            'title' => __('Select type of leaders'),
                                            'label' => __('Type of Leaders'),
                                            'list' => $permissions,
                                            'old' => $item->role->name ?? old('role'),
                                            'required' => true
                                        ]
                                    ])
                                    @endcomponent
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    {{ Form::label('description', 'Descrição') }}
                                    {{ Form::textarea('description', $item->description ?? old('description'), ['class' => 'form-control', 'id' => 'description', 'rows' => 4]) }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    @component('system::components.select', [
                                        'select' => [
                                            'name' => 'company',
                                            'title' => __('Select company for this department'),
                                            'label' => __('Company'),
                                            'list' => $companies,
                                            'old' => $company ?? old('company'),
                                            'required' => true
                                        ]
                                    ])
                                    @endcomponent
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    {{ Form::submit(__('Update'), ['class'=> 'btn btn-primary pull-right']) }}
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('components/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            $('select#users').selectpicker();
        })(jQuery);
    </script>
@endpush