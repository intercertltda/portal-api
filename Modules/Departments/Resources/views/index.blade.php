@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @php
                            $thead['name'] = __('Name');
                            $thead['companies'] = __('Company');
                            $thead['collaborators'] = __('Collaborators');
                            $thead['created_at'] = __('Created at');
                            $thead['actions'] = __('Actions');
                        @endphp
                        @component('system::components.table', [
                            'thead' => $thead
                        ])
                            @foreach($items as $department)
                                <tr>
                                    <td>{{ $department->name }}</td>
                                    <td>{{ $department->company->social_name }}</td>
                                    <td>{{ $department->collaborators }}</td>
                                    <td>{{ $department->created_at->format('d/m/Y') }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('departments.edit', $department->reference) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('departments.show', $department->reference) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                        <a href="#" data-local="departamentos" data-reference="{{ $department->reference }}" class="btn btn-danger js-confirm-action"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
