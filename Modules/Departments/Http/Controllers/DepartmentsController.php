<?php

namespace Modules\Departments\Http\Controllers;

use App\Classes\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Companies\Entities\Company;
use Modules\Companies\Facades\MyCompanies;
use Modules\Departments\Http\Requests\DepartmentRequest;
use Modules\Departments\Entities\Department as Model;
use Modules\Departments\Transformers\DepartmentResource as Resource;
use Modules\System\Facades\CompareRelationshipArray;
use Modules\Users\Entities\User;
use Spatie\Permission\Models\Role;

class DepartmentsController extends Controller
{
    /**
     * DepartmentsController constructor.
     */
    public function __construct()
    {
        $this->middleware(['forbid:responsible']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if (is_client()) {
            $departments = MyCompanies::departments();
        } else {
            $departments = Model::all();
        }

        return view('departments::index', [
            'items' => Resource::collection($departments),

            'title' => __('Departments'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'title' => __('Departments'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (is_client()) {
            $companies = MyCompanies::get();
            $collaborators = MyCompanies::collaborators();
        } else {
            $companies = Company::all();
            $collaborators = User::all();
        }

        $permissions = config('permissions.desc');

        unset($permissions['admin']);
        unset($permissions['directors']);

        return view('departments::create', [
            'collaborators' => $collaborators->count() > 0 ? Collection::setKey($collaborators, 'reference', 'name') : [],
            'companies' => $companies->count() > 0 ? Collection::setKey($companies, 'reference', 'social_name') : [],
            'permissions' => $permissions,

            'title' => __('Create'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('departments.index'),
                    'title' => __('Departments'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Create'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  DepartmentRequest $request
     * @return Response
     */
    public function store(DepartmentRequest $request)
    {
        $fields = (object) $request->all();

        $role = Role::where('name', $fields->role)
            ->first();

        $company = Company::reference($fields->company);

        $model = new Model;
        $model->name = $fields->name;
        $model->description = $fields->description;
        $model->role_id = $role->id;
        $model->company_id = $company->id;

        if ($model->save()) {
            if ($fields->responsible)
                foreach ($fields->responsible as $user)
                    $user = User::reference($user);
                    $model->users()->attach($user);
                    $user->assignRole($role->name);

            return redirect()
                ->route('departments.edit', $model->reference)
                ->with('success', __('Successfully created content.'));
        } else {
            return redirect()
                ->route('departments.create')
                ->withInput((array) $fields)
                ->with('error', __('Something went wrong.'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('departments::show', [
            'title' => __('Show'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('departments.index'),
                    'title' => __('Departments'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Show'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        $item = Model::reference($request->reference);

        if (is_client()) {
            $companies = MyCompanies::get();
            $collaborators = MyCompanies::collaborators();
        } else {
            $companies = Company::all();
            $collaborators = User::all();
        }

        foreach ($item->users as $user)
            $select[] = $user->reference;

        $permissions = config('permissions.desc');

        unset($permissions['admin']);
        unset($permissions['directors']);

        return view('departments::edit', [
            'item' => $item,
            'collaborators' => $collaborators->count() > 0 ? Collection::setKey($collaborators, 'reference', 'name') : [],
            'companies' => $companies->count() > 0 ? Collection::setKey($companies, 'reference', 'social_name') : [],
            'permissions' => $permissions,
            'select' => isset($select) ? array_unique($select) : [],
            'company' => $item->company->reference,

            'title' => __('Edit'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('departments.index'),
                    'title' => __('Departments'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Edit'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $fields = (object) $request->all();

        $compare = (object) CompareRelationshipArray::check($fields->old_responsible, $fields->responsible);

        $model = Model::reference($request->reference);
        $model->name = $fields->name;
        $model->description = $fields->description;

        if ($fields->role !== $fields->old_role) {
            $role = Role::where('name', $fields->role)
                ->first();
            $model->role_id = $role->id;
        }


        foreach ($model->users as $users)
            $inModel[] = $users->reference;

        if ($model->save()) {


            if (count($compare->removal) > 0)
            {
                foreach ($compare->removal as $forRemoval)
                {
                    $userRemoval = User::reference($forRemoval);
                    $userRemoval->removeRole($fields->old_role);
                }

            }

            if (count($compare->news) > 0)
            {
                foreach ($compare->news as $forAttach)
                {
                    $userAttach = User::reference($forAttach);
                    $model->users()->sync($userAttach->id);
                    $userAttach->assignRole($fields->old_role);
                }
            }

            return redirect()
                ->route('departments.edit', $request->reference)
                ->with('success', __('Content updated successfully.'));
        } else {
            return redirect()
                ->route('departments.edit', $request->reference)
                ->with('error', __('This item could not be saved.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Model::reference($request->reference);

        if (!$model->count())
            abort('404', __('Resource not found.'));

        if ($model->delete()) {
            return redirect()
                ->route('departments.index')
                ->with('success', __('Item moved to trash.'));
        } else {
            return redirect()
                ->route('departments.index')
                ->with('error', __('This item could not be moved to the trash.'));
        }
    }
}
