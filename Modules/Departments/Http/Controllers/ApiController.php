<?php

namespace Modules\Departments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Departments\Entities\Department as Model;
use Modules\Departments\Http\Requests\DepartmentRequest;
use Modules\Departments\Transformers\DepartmentResource as Resource;
use Modules\Users\Entities\User;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() : Response
    {
        return response()
            ->json(Resource::collection(Model::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  DepartmentRequest $request
     * @return Response
     */
    public function store(DepartmentRequest $request) : Response
    {
        $fields = (object) $request->all();

        $model = new Model;
        $model->name = $fields->name;
        $model->description = $fields->description;

        if ($model->save()) {
            if ($fields->users)
                foreach ($fields->users as $user)
                    $user = User::reference($user);
            $model->users()->attach($user);

            return response()
                ->json(new Resource($model), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.')
                ], 402);
        }
    }

    /**
     * Show the specified resource.
     * @param Response $response
     * @param string $reference
     * @return Response
     */
    public function show(Response $response, string $reference) : Response
    {
        return response()
            ->json(new Resource(Model::reference($reference)), 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(DepartmentRequest $request) : Response
    {
        $fields = (object) $request->all();

        $model = Model::reference($request->reference);
        $model->name = $fields->name;
        $model->description = $fields->description;

        if ($model->save()) {
            return response()
                ->json(new Resource($model), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.')
                ], 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request) : Response
    {
        $model = Model::reference($request->reference);

        if (!$model->count())
            return response()
                ->json(['message' => __('Resource not found.')], 402);

        if ($model->delete()) {
            return response()
                ->json([
                    'message' => __('Item moved to trash.')
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be moved to the trash.')
                ], 402);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function attach(Request $request) : Response
    {
        $fields = (object) $request->all();

        if ($request->ajax() && isset($fields->users)) {
            $model = Model::reference($request->reference);
            if (is_array($fields->users)) {
                foreach ($fields->users as $user)
                    $user = User::reference($user);
                    $model->users()->attach($user);
            } else {
                $user = User::reference($fields->users);
                $model->users()->attach($user);
            }
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function detach(Request $request) : Response
    {
        $fields = (object) $request->all();

        if ($request->ajax() && isset($fields->users)) {
            $model = Model::reference($request->reference);
            if (is_array($fields->users)) {
                foreach ($fields->users as $user)
                    $user = User::reference($user);
                $model->users()->detach($user);
            } else {
                $user = User::reference($fields->users);
                $model->users()->detach($user);
            }
        }
    }
}
