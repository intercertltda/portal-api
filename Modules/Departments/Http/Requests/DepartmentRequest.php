<?php

namespace Modules\Departments\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'responsible' => 'required|array',
            'role' => 'required'
        ];
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        return [
            'name.required' => 'O nome do Departamento é obrigatório.',
            'description.required' => 'A descrição do Departamento é obrigatório.',
            'responsible.required' => 'Precisa selecionar pelo menos um usuário como responsável',
            'responsible.array' => 'Os responsáveis precisa ser uma lista',
            'role.required' => 'O tipo de permissão é obrigatório.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
