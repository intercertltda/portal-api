<?php

return [
    'name' => 'Manager',


    'processes' => [
        'types' => [
            // se o campo ter "upload" deve conter também um campo para informação

            'text', // Texto simples - Informativo
            'field text', // Campo de Texto Simples
            'field date', // Campo de data
            'field number', // Campo de número,
            'field email', // Campo de E-mail


            'select', // Campos de select
            'checkbox', // Campos de checkbox
            'radio', // Campos de Radio
            'textarea', // Campo de textarea
            'upload files', // Upload Imagens,
            'upload information', // Upload de Arquivo Informativos = Vídeos, Imagens, PDF
        ],

        'description' => [
            'text' => 'Texto Informativo',
            'field' => 'Campo de texto',
            'checkbox' => 'Campo de escolha múltipla',
            'radio' => 'Campo de escolha única',
            'textarea' => 'Campo de mensagem',
            'upload files' => 'Upload de Arquivos',
            'upload information' => 'Upload de Informações'
        ]
    ]
];
