<?php

namespace Modules\Manager\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Departments\Transformers\DepartmentResource;
use Modules\Users\Transformers\UsersRelationship;

class Manager extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'title' => $this->title,
            'description' => $this->description,
            'time_execution' => $this->time_execution,
            'order' => $this->order,
            'created_at' => $this->created_at->format('d/m/Y \à\s H:i'),
            'department' => new DepartmentResource($this->department),
            'responsible' => UsersRelationship::collection($this->responsible)
        ];
    }
}
