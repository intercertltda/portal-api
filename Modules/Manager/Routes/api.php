<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/manager', function (Request $request) {
    return $request->user();
});


$params = [
    'prefix' => 'gestao',
    'as' => 'manager.'
];

Route::group($params, function () {
    Route::group([
        'prefix' => 'etapas',
        'as' => 'steps.'
    ], function () {
        Route::get('', 'Steps\ApiController@index')->name('index');
        Route::get('{reference}', 'Steps\ApiController@show')->name('show');
        Route::post('', 'Steps\ApiController@store')->name('store');
        Route::post('{reference}', 'Steps\ApiController@store')->name('update');
        Route::delete('{reference}', 'Steps\ApiController@destroy')->name('destroy');
    });


    Route::group([
        'prefix' => 'processos',
        'as' => 'process.'
    ], function () {
        Route::get('', 'Process\ApiController@index')->name('index');
        Route::get('{reference}', 'Process\ApiController@show')->name('show');
        Route::post('', 'Process\ApiController@store')->name('store');
        Route::post('{reference}', 'Process\ApiController@store')->name('update');
        Route::delete('{reference}', 'Process\ApiController@destroy')->name('destroy');
    });
});