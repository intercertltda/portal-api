<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Modules\System\Facades\RouteFacade;

RouteFacade::group([
    'prefix' => 'etapas/processos',
    'middleware' => ['auth'],
    'as' => 'steps.processes.'
], 'Process\MainController');


Route::get('etapas/ordenacao', 'ManagerController@index')->name('manager.order');

RouteFacade::group([
    'prefix' => 'etapas',
    'middleware' => ['auth'],
    'as' => 'steps.'
], 'Steps\MainController');