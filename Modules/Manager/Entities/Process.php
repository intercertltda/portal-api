<?php

namespace Modules\Manager\Entities;;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Reference;
use App\Traits\ModelHelpers;
use Modules\Users\Entities\User;

class Process extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    protected $table = 'processes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'time_execution'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function responsible() : BelongsToMany
    {
        return $this->belongsToMany(User::class, 'processes_responsible', 'process_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function step()
    {
        return $this->hasOne(Step::class, 'id', 'step_id');
    }

    /**
     * @return mixed
     */
    public function getDataAttribute()
    {
        return json_decode($this->attributes['data']);
    }
}
