<?php

namespace Modules\Manager\Entities;

use Illuminate\Database\Eloquent\Model;

class Solicitation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'status', 'type', 'description', 'collaborator_id', 'agent_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'status', 'type', 'collaborator_id', 'agent_id'
    ];

    public function collaborator(){
      return $this->belongsToMany(Collaborator::class);
    }

    public function agent(){
      return $this->belongsTo(Agent::class);
    }
}
