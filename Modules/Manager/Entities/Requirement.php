<?php

namespace Modules\Manager\Entities;;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'department_id', 'step_id'
    ];

    public function department(){
      return $this->belongsToMany(Department::class);
    }

    public function step(){
      return $this->belongsToMany(Step::class);
    }
}
