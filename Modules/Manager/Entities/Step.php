<?php

namespace Modules\Manager\Entities;;

use App\Classes\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Reference;
use App\Traits\ModelHelpers;
use Modules\Companies\Entities\Company;
use Modules\Departments\Entities\Department;
use Modules\Users\Entities\User;

class Step extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    protected $table = 'steps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'time_execution',
        'depends_id',
        'department_id',
        'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [ 'responsible_names' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function responsible() : BelongsToMany
    {
        return $this->belongsToMany(User::class, 'steps_responsible', 'step_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function depends()
    {
        return $this->hasOne(Step::class, 'id', 'depends_id');
    }

    /**
     * @return string
     */
    public function getResponsibleNamesAttribute()
    {
        $names = Collection::getColumn($this->responsible, 'name');

        return implode(', ', $names);
    }
}
