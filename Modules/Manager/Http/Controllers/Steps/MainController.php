<?php

namespace Modules\Manager\Http\Controllers\Steps;

use App\Classes\Collection;
use App\Classes\Find;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Modules\Companies\Entities\Company;
use Modules\Companies\Facades\MyCompanies;
use Modules\Departments\Entities\Department;
use Modules\Manager\Entities\Step;
use Modules\Manager\Http\Requests\StepRequest;
use Modules\Manager\Transformers\Manager as Resource;
use Modules\Manager\Entities\Step as Model;
use Modules\System\Facades\CompareRelationshipArray;
use Modules\Users\Entities\User;

class MainController extends Controller
{
    /**
     * MainController constructor.
     */
    public function __construct()
    {
        $this->middleware(['forbid:responsible']);
    }

    /**
     * @return array
     */
    public function data()
    {
        if (is_client()) {
            return [
                'companies' => MyCompanies::get(),
                'depends' => MyCompanies::steps(),
                'departments' => MyCompanies::departments(),
                'collaborators' => MyCompanies::collaborators()
            ];
        } else {
            return [
                'companies' => Company::all(),
                'depends' => Step::all(),
                'departments' => Department::all(),
                'collaborators' => User::all()
            ];
        }
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if (is_client()) {
            $steps = MyCompanies::steps();
        } else {
            $steps = Model::all();
        }

        return view('manager::steps.index', [
            'items' => Resource::collection($steps),

            'title' => __('Steps'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'title' => __('Steps'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data = (object) $this->data();

        return view('manager::steps.create', [
            'collaborators' => Collection::setKey($data->collaborators, 'reference', 'name'),
            'departments' => Collection::setKey($data->departments, 'reference', 'name'),
            'depends' => $data->depends->count() > 0 ? Collection::setKey($data->depends, 'reference', 'title') : [],
            'companies' => Collection::setKey($data->companies, 'reference', 'social_name'),

            'title' => __('Create'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('steps.index'),
                    'title' => __('Processes management'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Create'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  StepRequest $request
     * @return Response
     */
    public function store(StepRequest $request)
    {
        $fields = (object) $request->all();

        $model = Model::create([
            'title' => $fields->title,
            'time_execution' => $fields->time_execution ?? 1,
            'description' => $fields->description,
            'depends_id' => Find::db('steps', ['reference' => $fields->depend]),
            'department_id' => Find::db('departments', ['reference' => $fields->department]),
            'company_id' => Find::db('companies', ['reference' => $fields->company])
        ]);

        if ($model) {
            foreach ($fields->collaborators as $reference) {
                $model->responsible()->attach(Find::db('users', ['reference' => $reference]));
            }

            return redirect()
                ->route('steps.edit', $model->reference)
                ->with('success', __('Successfully created content.'));
        } else {
            return redirect()
                ->route('steps.create')
                ->withInput((array)$fields)
                ->with('error', __('Something went wrong.'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('manager::show', [
            'title' => __('Show'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('steps.index'),
                    'title' => __('Processes management'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Show'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @param string $reference
     * @return Response
     */
    public function edit(Request $request)
    {
        $data = (object) $this->data();
        $model = Model::reference($request->reference);

        $newDepends = $data->depends->filter(function ($step) use ($request, $model) {
            return $step->reference != $request->reference  and $step->order < $model->order;
        });

        return view('manager::steps.edit', [
            'item' => $model,
            'collaborators' => Collection::setKey($data->collaborators, 'reference', 'name'),
            'departments' => Collection::setKey($data->departments, 'reference', 'name'),
            'depends' => $newDepends->count() > 0 ? Collection::setKey($newDepends, 'reference', 'title') : [],
            'companies' => Collection::setKey($data->companies, 'reference', 'social_name'),

            'selected' => (object) [
                'collaborators' => Collection::getColumn($model->responsible, 'reference')
            ],

            'title' => __('Edit'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('steps.index'),
                    'title' => __('Processes management'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Edit'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(StepRequest $request)
    {
        $fields = (object) $request->all();
        $compare = (object) CompareRelationshipArray::check($fields->old_collaborators, $fields->collaborators);

        $companyID = Find::db('companies', ['reference' => $fields->company]);

        $model = Model::reference($request->reference)->update([
            'title' => $fields->title,
            'time_execution' => $fields->time_execution ?? 1,
            'description' => $fields->description,
            'depends_id' => Find::db('steps', ['reference' => $fields->department]),
            'department_id' => Find::db('departments', ['reference' => $fields->department]),
            'company_id' => $companyID,
            'order' => DB::table('steps')->where('company_id', $companyID)->orderBy('created_at')->count() + 1
        ]);

        if ($model) {
            if (count($compare->news) > 0)
            {
                foreach ($compare->news as $forAttach)
                {
                    $userAttach = User::reference($forAttach);
                    $model->responsible()->sync($userAttach->id);
                }
            }

            return redirect()
                ->route('steps.edit', $request->reference)
                ->with('success', __('Successfully updated content.'));
        } else {
            return redirect()
                ->route('steps.edit', $request->reference)
                ->withInput((array)$fields)
                ->with('error', __('Something went wrong.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
    }
}
