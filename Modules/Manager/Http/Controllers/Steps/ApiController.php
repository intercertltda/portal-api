<?php

namespace Modules\Manager\Http\Controllers\Steps;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Modules\Manager\Entities\Step;
use Modules\Manager\Http\Requests\StepRequest;
use Modules\Manager\Transformers\Manager as Resource;
use Modules\Manager\Entities\Step as Main;
use Modules\Users\Entities\User;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return response()
            ->json(Resource::collection(Main::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  StepRequest $request
     * @return Response
     */
    public function store(StepRequest $request)
    {
        $fields = (object) $request->all();

        $department = DB::table('departments')
            ->where('reference', $fields->department)
            ->select('id')
            ->first();

        $model = new Step();

        $model->title = $fields->title;
        $model->description = $fields->description;
        $model->department_id = $department->id;
        $model->order = $fields->order;

        if ($fields->depend) {
            $step = DB::table('steps')
                ->where('reference', $fields->department)
                ->select('id')
                ->first();

            $model->depends_id = $step->id;
        }

        if ($model->save()) {
            if ($fields->responsible)
                foreach ($fields->responsible as $user)
                    $user = User::reference($user);
                    $model->responsible()->attach($user);

            return response()
                ->json(new Resource($model), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.')
                ], 402);
        }
    }

    /**
     * Show the specified resource.
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
        return response()
            ->json(new Resource(Main::reference($request->reference)));
    }

    /**
     * Update the specified resource in storage.
     * @param StepRequest $request
     * @return Response
     */
    public function update(StepRequest $request)
    {
        $fields = (object) $request->all();

        $model = Step::reference($request->reference);

        $model->title = $fields->title;
        $model->description = $fields->description;
        $model->department_id = $fields->department;
        $model->order = $fields->order;

        if ($model->save()) {
            if ($fields->responsible)
                foreach ($fields->responsible as $user)
                    $ids[] = User::reference($user)->id;

            $model->responsible()->sync($ids);

            return response()
                ->json(new Resource($model), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.')
                ], 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Step::reference($request->reference);

        if (!$model->count())
            return response()
                ->json(['message' => __('Resource not found.')], 402);

        if ($model->delete()) {
            return response()
                ->json([
                    'message' => __('Item moved to trash.')
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be moved to the trash.')
                ], 402);
        }
    }
}