<?php

namespace Modules\Manager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StepRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'title' => 'required',
            'time_execution' => 'required|numeric|min:1',
            'description' => 'required',
            'department' => 'required',
            'collaborators' => 'required|array|min:1'
        ];
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        return [
            'title' => 'O título é obrigatório',
            'description' => 'A descrição é obrigatória',
            'department' => 'O departamento é obrigatório',

            'time_execution.required' => 'O campo de tempo é obrigatório',
            'time_execution.numeric' => 'O campo de tempo deve conter apenas números',
            'time_execution.min' => 'O campo de tempo deve ter pelo menos 1 dia',

            'collaborators.required' => 'Deve ter um responsável',
            'collaborators.array' => 'O campo responsável deve ser uma ARRAY',
            'collaborators.min' => 'Deve ter pelo menos um responsável',
            'collaborators.string' => 'O responsável deve ser uma string'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
