@extends('system::layouts.master')
@push('push-buttons')
    <a href="{{ route('steps.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('Create') @lang('Step')</a>
    <a href="{{ route('steps.processes.create', ['reference' => $item->reference]) }}" class="btn btn-info"><i class="fa fa-plus"></i> @lang('Create') @lang('Process')</a>
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                            'url' => route('steps.update', $item->reference)
                        ]) }}
                        {{ Form::hidden('__method', 'PUT') }}

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="pull-left">@lang('Data of Step')</h4>
                            </div>
                        </div>
                        <br>
                        <div class="row form-group">
                            <div class="col-md-4">
                                {{ Form::label('title', __('Title')) }}
                                {{ Form::text('title', old('title') ?? $item->title, ['class' => 'form-control', 'id' => 'title', 'required']) }}
                            </div>

                            <div class="col-md-4">
                                {{ Form::label('time_execution', sprintf('%s (%s)', __('Time of execution'), __('in days'))) }}
                                {{ Form::number('time_execution', old('time_execution') ?? $item->time_execution ?? '1', ['class' => 'form-control', 'id' => 'time_execution', 'required', 'min' => '1']) }}
                            </div>

                            <div class="col-md-4">
                                @foreach($selected->collaborators as $i)
                                    {{ Form::hidden('old_collaborators[]', $i) }}
                                @endforeach

                                @component('system::components.select', [
                                    'select' => [
                                        'name' => 'collaborators',
                                        'name-array' => 'collaborators[]',
                                        'title' => __('Select user responsible'),
                                        'label' => __('Responsible'),
                                        'list' => $collaborators,
                                        'old' => old('collaborators') ?? $selected->collaborators,
                                        'multiple' => true,
                                        'required' => true
                                    ]
                                ])
                                @endcomponent
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                @component('system::components.select', [
                                    'select' => [
                                        'name' => 'department',
                                        'title' => __('Select department'),
                                        'label' => __('Department'),
                                        'list' => $departments,
                                        'old' => old('department') ?? $item->department->reference,
                                        'required' => true
                                    ]
                                ])
                                @endcomponent
                            </div>

                            <div class="col-md-4">
                                @component('system::components.select', [
                                    'select' => [
                                        'name' => 'depend',
                                        'title' => __('Select step'),
                                        'label' => __('Depends by'),
                                        'list' => $depends,
                                        'old' => old('depend') ?? $item->depends->reference ?? null
                                    ]
                                ])
                                @endcomponent
                            </div>

                            <div class="col-md-4">
                                @component('system::components.select', [
                                    'select' => [
                                        'name' => 'company',
                                        'title' => __('Select company for this department'),
                                        'label' => __('Company'),
                                        'list' => $companies,
                                        'old' => old('company') ?? $item->company->reference,
                                        'required' => true
                                    ]
                                ])
                                @endcomponent
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                {{ Form::label('description', __('Description')) }}
                                {{ Form::textarea('description', old('description') ?? $item->description, ['class' => 'form-control description', 'id' => 'description', 'required', 'rows' => '4']) }}
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">@lang('Update')</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
