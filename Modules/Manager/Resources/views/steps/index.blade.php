@extends('system::layouts.master')
@push('push-buttons')
    <a href="{{ route('steps.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('Create') @lang('Step')</a>
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @component('system::components.table', [
                            'thead' => [
                                'name' => __('Name'),
                                'department' => __('Department'),
                                'responsible' => __('Responsible'),
                                'actions' => __('Actions')
                            ]
                        ])
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->department->name }}</td>
                                    <td>{{ $item->responsible_names }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('steps.edit', $item->reference) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('steps.show', $item->reference) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                        <a href="#" data-local="etapas" data-reference="{{ $item->reference }}" class="btn btn-danger js-confirm-action"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
