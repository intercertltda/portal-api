@extends('layouts.admin')
@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="page-breadcrumb">
   <div class="row">
       <div class="col-12 d-flex no-block align-items-center">
           <h4 class="page-title">Solicitações</h4>
           <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                   <ol class="breadcrumb">
                       <li class="breadcrumb-item"><a href="#">Admin</a></li>
                       <li class="breadcrumb-item active" aria-current="page">Solicitações</li>
                   </ol>
               </nav>
           </div>
       </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
   <!-- ============================================================== -->
   <!-- Sales Cards  -->
   <!-- ============================================================== -->
   <div class="row">
     <div class="col-md-3">
       <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ver solicitações em aberto">
         <div class="card text-white bg-primary">
           <div class="card-body">
             <h4>Abertas</h4>
             <h6>2</h6>
           </div>
         </div>
       </a>
     </div>
     <div class="col-md-3">
       <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ver solicitações em atendimento">
        <div class="card text-white bg-info">
          <div class="card-body">
            <h4>Em Atendimento</h4>
            <h6>1</h6>
          </div>
        </div>
      </a>
     </div>
     <div class="col-md-3">
      <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ver solicitações em encerradas">
       <div class="card text-white bg-success">
         <div class="card-body">
           <h4>Encerradas</h4>
           <h6>4</h6>
         </div>
       </div>
      </a>
     </div>
     <div class="col-md-3">
       <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ver solicitações em canceladas">
         <div class="card text-white bg-danger">
           <div class="card-body">
             <h4>Canceladas</h4>
             <h6>6</h6>
           </div>
         </div>
       </a>
     </div>
   </div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="zero_config" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Tipo</th>
                <th>Agente</th>
                <th>Criação em</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              @foreach($solicitations->where('status','open') as $solicitation)
              <tr>
                <td>{{$solicitation->agent->profile->name}}</td>
                <td>{{$solicitation->created_at->format('d/m/Y')}}</td>
                <td class="text-center">
                  <button type="button" name="button" class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
  $("#zero_config").DataTable();
</script>
@endsection
