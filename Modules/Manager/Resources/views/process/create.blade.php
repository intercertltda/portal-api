@extends('layouts.admin')
@section('css')
<style media="screen">
  a:hover{
    underline:none;
  }
</style>
@endsection
@section('content')
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Novo Processo</h4>
      <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('admin')}}">Admin</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{ url('admin/processo')}}">Processos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Novo</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <nav class="nav nav-pills nav-fill">
            <a class="nav-item nav-link active" id="title">Título</a>
            <a class="nav-item nav-link" id="department">Departamento</a>
            <a class="nav-item nav-link" id="requirement">Requisitos</a>
            <a class="nav-item nav-link" id="extra">Extras</a>
          </nav>
        </div>
        <div class="card-body">
          <form action="{{ url('admin/processo')}}" method="post">
            {{ csrf_field() }}
            <div class="tab-content">
              <div class="tab-pane fade show active" id="tabTitle" role="tabpanel" aria-labelledby="home-tab">
                <div class="row form-group">
                  <div class="col-md-12">
                    <label for="title">Título</label>
                    <input type="text" name="title" value="" class="form-control" required>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-12">
                    <label for="description">Descrição</label>
                    <textarea name="description" value="" class="form-control"></textarea>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tabDepartment" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row form-group">
                  <div class="col-md-12">
                    <label for="title">Departamento</label>
                    <select class="form-control" name="department_id" required>
                      <option value="">Selecione</option>
                      @foreach($departments as $department)
                      <option value="{{$department->id}}">{{$department->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tabRequirement" role="tabpanel" aria-labelledby="contact-tab">

              </div>
              <div class="tab-pane fade" id="tabExtra" role="tabpanel" aria-labelledby="contact-tab">

              </div>
            </div>
            <div class="row form-group ">
              <div class="col-md-12">
                <center>
                  <button type="button" role="button" name="back" class="btn btn-default" style="display:none;" value="0"><i class="fa fa-angle-left align-middle"></i> Voltar</button> <button type="button" role="button" name="next" class="btn btn-primary" value="1">Próximo <i class="fa fa-angle-right align-middle"></i></button>
                </center>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $('button[name=next]').click(function(){
    switch ($(this).val()) {
      case 1:
        if($('input[name=title]').val() != null){
          $('#title').removeClass('active')
          $('#department').addClass('active');
          $('button[name=back]').val(1);
          $('button[name=back]').show();
          hideAllTabs();
          $('#tabDepartment').show();
          $(this).val(2);
        }else{
          toastr.warning("O Campo título é obrigatório");
        }
        break;

      case 2:
        break;

      case 3:

        break;

      default:

    }
  });

  function hideAllTabs(){
    $("#tabTitle").hide();
    $("#tabDepartment").hide();
    $("#tabRequirement").hide();
    $("#tabExtra").hide();
  }

  $('button[name=back]').click(function(){
    switch (expression) {
      case $(this).val() == 4:

        break;
      default:

    }
  });
</script>
@endsection
