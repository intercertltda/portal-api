<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')
                ->unsigned();

            $table->integer('depends_id')
                ->unsigned()
                ->nullable();

            $table->integer('department_id')
                ->unsigned();

            $table->integer('order')
                ->default(1)
                ->nullable();

            $table->string('title');
            $table->string('description')->nullable();
            $table->string('time_execution')->nullable();

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('steps', function (Blueprint $table) {
            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('depends_id')
                ->references('id')
                ->on('steps')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
