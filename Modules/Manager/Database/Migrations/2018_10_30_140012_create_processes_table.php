<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = config('manager.processes.types');

        Schema::create('processes', function (Blueprint $table) use ($types) {
            $table->increments('id');

            $table->integer('step_id')->unsigned();
            $table->string('type')->default(array_first($types));
            $table->string('title');
            $table->string('description');
            $table->integer('time_execution')->nullable();
            $table->integer('order')->default(1);

            /*
             * Tipos:
             * text: {"content": "Aqui é uma descrição para o tipo 'text'", "label": "Informação Básica", "name": "information"}
             * field text: {"content": null, "label": "Campo para preencher nome", "name": "name"}
             * field date: {"content": null, "label": "Campo para preencher data", "name": "along_config"}
             * field number: {"content": null, "label": "Campo para preencher um número", "name": "number"}
             * field email: {"content": null, "label": "Campo para preencher um email", "name": "email"}
             *
             * checkbox: {"content": null, "label": "Campo de múltiplas escolhas", "name": "checkbox", "options":[{"label": "Opção 1", "value": "op1"}, {"label": "Opção 2", "value": "op2"}]}
             * select: {"content": null, "label": "Campo de múltiplas escolhas", "name": "select", "options":[{"label": "Opção 1", "value": "op1"}, {"label": "Opção 2", "value": "op2"}]}
             * radio: {"content": null, "label": "Campo de múltiplas escolhas", "name": "radio", "options":[{"label": "Opção 1", "value": "op1"}, {"label": "Opção 2", "value": "op2"}]}
             *
             * textarea: {"content": null, "label": "Campo para preencher um texto qualquer", "name": "textarea"}
             * upload files: {"content": null, "label": "Campo para upload de arquivos", "name": "uploads", "multiple": true}
             * upload information: {"content": "url-para-o-arquivo", "label": "Campo para upload de arquivos informativos", "name": "uploads_information", "multiple": false}
             */
            $table->longText('data');

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::table('processes', function (Blueprint $table) {
            $table->foreign('step_id')
                ->references('id')
                ->on('steps')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes');
    }
}
