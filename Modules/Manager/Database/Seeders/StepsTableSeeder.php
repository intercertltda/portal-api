<?php

namespace Modules\Manager\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Manager\Entities\Step;
use Modules\Users\Entities\User;

class StepsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $steps = [
            [
                'name' => 'Origem do Contato',
                'description' => 'Onde o Usuário define a origem do contato',
                'department_id' => 1
            ],
            [
                'name' => 'Vídeo de Apresentação',
                'description' => 'Apresentação do negócio',
                'department_id' => 2
            ]
        ];

        $i = 1;
        foreach (toObject($steps) as $step)
        {
            $model = new Step();

            $model->title = $step->name;
            $model->description = $step->description;
            $model->department_id = $step->department_id;
            $model->order = $i;

            $model->save();

            $user = User::where('login', 'felipe.rank')->first();
            $model->responsible()->attach($user);

            $i++;
        }
    }
}
