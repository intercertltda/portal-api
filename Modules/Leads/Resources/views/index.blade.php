@extends('resources.views.layouts.admin')
@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="page-breadcrumb">
   <div class="row">
       <div class="col-12 d-flex no-block align-items-center">
           <h4 class="page-title">Leads</h4>
           <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                   <ol class="breadcrumb">
                       <li class="breadcrumb-item"><a href="#">Admin</a></li>
                       <li class="breadcrumb-item active" aria-current="page">Leads</li>
                   </ol>
               </nav>
           </div>
       </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
   <!-- ============================================================== -->
   <!-- Sales Cards  -->
   <!-- ============================================================== -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="zero_config" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Criação em</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              @foreach($leads as $lead)
              <tr>
                <td>{{$leads->profile->name}}</td>
                <td>{{$leads->profile->phone}}</td>
                <td>{{$leads->created_at->format('d/m/Y')}}</td>
                <td class="text-center">
                  <button type="button" name="button" class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
  $("#zero_config").DataTable();
</script>
@endsection
