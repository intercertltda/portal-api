<?php

namespace Modules\Leads\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Calls\Entities\Call;

class Lead extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'interested'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function calls()
    {
        return $this->morphMany(Call::class, 'callable');
    }
}
