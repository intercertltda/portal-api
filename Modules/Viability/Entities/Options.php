<?php

namespace Modules\Manager\Entities;;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table = 'questions_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'ask_id'
    ];
}
