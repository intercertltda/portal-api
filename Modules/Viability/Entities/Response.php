<?php

namespace Modules\Manager\Entities;;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = 'questions_responses';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'response'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'responseable_id', 'responseable_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function responseable()
    {
        return $this->morphTo();
    }


    public function getReponseAttribute()
    {
        return json_decode($this->attributes['response']);
    }

    public function note()
    {

    }
}
