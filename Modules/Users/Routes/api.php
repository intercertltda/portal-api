<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\System\Facades\RouteFacade;

Route::middleware(['auth:api', 'role:admin|directors'])
    ->get('colaboradores/clients', 'ApiController@clients')
    ->name('api.users.clients');

RouteFacade::api([
    'prefix' => 'colaboradores',
    'middleware' => ['auth:api', 'role:admin|directors'],
    'as' => 'api.users.'
], 'ApiController');