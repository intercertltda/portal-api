<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\System\Facades\RouteFacade;

Route::group([
    'prefix' => 'perfil',
    'as' => 'profile.'
], function () {
    Route::get('', 'ProfileController@index')->name('index');
    Route::post('', 'ProfileController@update')->name('update');
});

RouteFacade::group([
    'prefix' => 'colaboradores',
    'middleware' => ['auth', 'role:admin|directors'],
    'as' => 'users.'
], 'UsersController');
