<?php

namespace Modules\Users\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ProfileResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type_other_document' => $this->type_other_document,
            'other_document' => $this->other_document,
            'phone' => $this->phone,
            'genre' => $this->genre,
        ];
    }
}
