<?php

namespace Modules\Users\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class UsersRelationship extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'name' => $this->name,
            'email' => $this->email,
            'login' => $this->login,
            'document' => (string) $this->document,
            'insert_at' => $this->pivot->created_at->format('d/m/Y \à\s H:i')
        ];
    }
}
