<?php

namespace Modules\Users\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Addresses\Transformers\AddressResource;

class UsersResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'name' => $this->name,
            'login' => $this->login,
            'email' => $this->email,
            'document' => (string) $this->document,
            'type' => $this->type,
            'active' => $this->enabled,
            'logged_at' => isset($this->logged_at) ? helpers()->date($this->logged_at, 'd/m/Y \à\s H:i') : __('Not yet entered.'),
            'profile' => new ProfileResource($this->profile),
            'address' => new AddressResource($this->profile->address)
        ];
    }
}
