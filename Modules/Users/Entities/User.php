<?php

namespace Modules\Users\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Modules\Calls\Entities\Call;
use Modules\Companies\Entities\Company;
use Modules\Manager\Entities\Comment;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens,
        ModelHelpers,
        SoftDeletes,
        Notifiable,
        Reference,
        HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'enabled', 'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function profile()
    {
        return $this->morphOne(Profile::class, 'profileable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function calls()
    {
        return $this->morphMany(Call::class, 'callable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function responsible()
    {
        return $this->belongsToMany(Company::class, 'companies_responsible', 'user_id', 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function representative()
    {
        return $this->hasMany(Company::class, 'representative_id');
    }

    /**
     * @return mixed
     */
    public function getCompaniesAttribute()
    {
        $responsible = $this->responsible()->latest()->get();
        $representative = $this->representative()->latest()->get();

        return $responsible->merge($representative);
    }
}
