<?php

namespace Modules\Users\Http\Requests;

use App\Rules\ValidateDocument;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Addresses\Http\Requests\AddressArray;

class ProfileUpdateRequest extends FormRequest
{
    protected $requests = [
        AddressArray::class,
        ProfileRequest::class,
        UsersRequest::class
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $user = $this->user();

        $rules['login'] = 'required|unique:users,login,' . $user->id;
        $rules['email'] = 'required|email|unique:users,email,' . $user->id;
        $rules['document'] = ['required', 'unique:users,document,' . $user->id, new ValidateDocument()];
        $rules['password'] = 'confirmed';

        foreach($this->requests as $source)
        {
            $rules = array_merge($rules, (new $source)->rules());
        }

        return $rules;
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        $messages = [];

        foreach($this->requests as $source)
        {
            $messages = array_merge($messages, (new $source)->messages());
        }

        return $messages;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
