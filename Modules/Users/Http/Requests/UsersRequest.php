<?php

namespace Modules\Users\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => 'required'
        ];
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        return [
            'reference'          => 'A Referência é obrigatória',
            'name.required'      => 'Nome é obrigatório',

            'login.required'     => 'Login é obrigatório',
            'login.unique'     	 => 'Esse login já existe na base de dados',

            'email.required'     => 'E-mail é obrigatório',
            'email.email'        => 'E-mail precisa ser válido',
            'email.unique'       => 'Esse e-mail já existe na base de dados',

            'document.required'  => 'O documento é obrigatório',
            'document.unique'    => 'Esse doumento já existe',
            'document.valid'     => 'O documento precisa ser válido',

            'password.required'  => 'A senha é obrigatória',
            'password_confirmation.required'  => 'A confirmação de senha é obrigatória',
            'password.confirmed' => 'As senhas não coincidem'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
