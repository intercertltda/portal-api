<?php

namespace Modules\Users\Http\Requests;

use App\Rules\ValidateDocument;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Addresses\Http\Requests\AddressArray;

class UserStoreRequest extends FormRequest
{
    protected $requests = [
        AddressArray::class,
        ProfileRequest::class,
        UsersRequest::class
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $rules = [
            'login' => 'required|unique:users,login',
            'email' => 'required|email|unique:users,email',
            'document' => ['required', 'unique:users,document', new ValidateDocument()],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];

        foreach($this->requests as $source)
        {
            $rules = array_merge($rules, (new $source)->rules());
        }

        return $rules;
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        $messages = [
            'reference'          => 'A Referência é obrigatória',
            'name.required'      => 'Nome é obrigatório',

            'login.required'     => 'Login é obrigatório',
            'login.unique'     	 => 'Esse login já existe na base de dados',

            'email.required'     => 'E-mail é obrigatório',
            'email.email'        => 'E-mail precisa ser válido',
            'email.unique'       => 'Esse e-mail já existe na base de dados',

            'document.required'  => 'O documento é obrigatório',
            'document.unique'    => 'Esse doumento já existe',
            'document.valid'     => 'O documento precisa ser válido',

            'password.required'  => 'A senha é obrigatória',
            'password_confirmation.required'  => 'A confirmação de senha é obrigatória',
            'password.confirmed' => 'As senhas não coincidem'
        ];

        foreach($this->requests as $source)
        {
            $messages = array_merge($messages, (new $source)->messages());
        }

        return $messages;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
