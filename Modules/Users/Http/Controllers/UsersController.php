<?php

namespace Modules\Users\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Companies\Entities\Company;
use Modules\Users\Entities\User as Model;
use Modules\Users\Http\Requests\UserStoreRequest;
use Modules\Users\Http\Requests\UserUpdateRequest;
use Modules\Users\Transformers\UsersResource as Resource;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['forbid:responsible']);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (has_company() and !is_admin()) {
            foreach ($request->user()->companies as $company) {
                foreach ($company->users as $user)
                {
                    $users[] = $user;
                }
            }

            $collection = collect($users)->unique();
        } else {
            $collection = Model::all();
        }

        $resource = Resource::collection($collection);

        return view('users::index', [
            'items' => $resource,

            'title' => __('Collaborators'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'title' => __('Collaborators'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('users::create', [
            'title' => __('Create'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('users.index'),
                    'title' => __('Collaborators'),
                    'current' => false,
                    'icon' => 'fa fa-users'
                ],
                [
                    'title' => __('Create'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  UserStoreRequest $request
     * @return Response
     */
    public function store(UserStoreRequest $request)
    {
        $fields = (object) $request->all();

        $model = new Model();
        $model->name = $fields->name;
        $model->login = $fields->login;
        $model->email = $fields->email;
        $model->document = helpers()->clean($fields->document);
        $model->enabled = 'yes';
        $model->password = bcrypt($fields->password);

        if ($model->save()) {
            $profile = $model->profile()->create([
                'phone' => helpers()->clean($fields->profile['phone']),
                'genre' => $fields->profile['genre']
            ]);
            $profile->address()->create([
                'code_postal' => $fields->address['code_postal'],
                'street' => $fields->address['street'],
                'number' => $fields->address['number'],
                'complement' => $fields->address['complement'],
                'neighborhood' => $fields->address['neighborhood'],
                'state' => $fields->address['state'],
                'city' => $fields->address['city'],
            ]);
            $model->assignRole($fields->user_type);

            return redirect()
                ->route('users.edit', $model->reference)
                ->with('success', __('Content updated successfully.'));
        } else {
            return redirect()
                ->route('users.create')
                ->with('fail', __('Content could not be updated.'))
                ->withInput(toArray($fields));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('users::show', [
            'title' => __('Show'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('users.index'),
                    'title' => __('Collaborators'),
                    'current' => false,
                    'icon' => 'fa fa-users'
                ],
                [
                    'title' => __('Show'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        $user = Model::reference($request->reference);

        return view('users::edit', [
            'userData' => $user,

            'title' => __('Edit'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('users.index'),
                    'title' => __('Collaborators'),
                    'current' => false,
                    'icon' => 'fa fa-users'
                ],
                [
                    'title' => __('Edit'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  UserUpdateRequest $request
     * @return Response
     */
    public function update(UserUpdateRequest $request)
    {
        $fields = (object) $request->all();

        $model = Model::reference($request->reference);
        $model->name = $fields->name;
        $model->login = $fields->login;
        $model->email = $fields->email;
        $model->document = $fields->document;
        $model->enabled = 'yes';

        if (isset($fields->password)) {
            $model->password = bcrypt($fields->password);
        }

        if ($model->save()) {
            $model->profile->update([
                'phone' => helpers()->clean($fields->profile['phone']),
                'genre' => $fields->profile['genre']
            ]);

            $model->profile->address->update([
                'code_postal' => $fields->address['code_postal'],
                'street' => $fields->address['street'],
                'number' => $fields->address['number'],
                'complement' => $fields->address['complement'],
                'neighborhood' => $fields->address['neighborhood'],
                'state' => $fields->address['state'],
                'city' => $fields->address['city'],
            ]);
            $model->syncRoles($fields->user_type);

            return redirect()
                ->route('users.edit', $request->reference)
                ->with('success', __('Content updated successfully.'));
        } else {
            return redirect()
                ->route('users.edit', $request->reference)
                ->with('fail', __('Content could not be updated.'))
                ->withInput(toArray($fields));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Model::reference($request->reference);

        return $this->delete_method($request, $model, 'collaborators.index');
    }
}
