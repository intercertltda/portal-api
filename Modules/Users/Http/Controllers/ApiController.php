<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Entities\User as Model;
use Modules\Users\Http\Requests\UsersRequest;
use Modules\Users\Transformers\UsersResource;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return response()
            ->json(UsersResource::collection(Model::all()), 200);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function clients()
    {
        return response()
            ->json(UsersResource::collection(Model::where('type', 'client')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(UsersRequest $request)
    {
        $fields = (object) $request->all();

        $model = new Model();
        $model->name = $fields->name;
        $model->login = $fields->login;
        $model->email = $fields->email;
        $model->document = $fields->document;
        $model->enabled = 'yes';
        $model->password = bcrypt($fields->password);

        if ($model->save()) {
            $profile = $model->profile()->create($fields->profile);
            $profile->address()->create($fields->address);
            $model->assignRole($fields->user_type);

            return response()
                ->json($model->toArray(), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.')
                ], 402);
        }
    }

    /**
     * Show the specified resource.
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
        $model = Model::reference($request->reference);

        if (!$model->count())
            return response()
                ->json(['message' => __('Resource not found.')], 402);

        return response()
            ->json(new UsersResource($model), 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $reference)
    {
        $fields = (object) $request->all();
        $model = Model::reference($reference);

        if (!$model->count())
            return response()
                ->json(['message' => __('Resource not found.')], 402);

        $model->name = $fields->name;
        $model->login = $fields->login;
        $model->email = $fields->email;
        $model->document = $fields->document;
        $model->enabled = 'yes';

        if (isset($fields->password))
            $model->password = bcrypt($fields->password);

        if ($model->save()) {
            $model->profile->update($fields->profile);
            $model->profile->address->update($fields->address);


            if (isset($fields->user_type))
                foreach($model->getRoleNames() as $role)
                    $model->removeRole($role);

            $model->assignRole($fields->user_type);

            return response()
                ->json(new UsersResource($user), 200);
        } else {
            return response()
                ->json([
                    'message' => __('Could not update this item.')
                ], 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Model::reference($request->reference);

        if (!$model->count())
            return response()
                ->json(['message' => __('Resource not found.')], 402);

        if ($model->delete()) {
            return response()
                ->json([
                    'message' => __('Item moved to trash.')
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be moved to the trash.')
                ], 402);
        }
    }
}
