<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Http\Requests\ProfileUpdateRequest;
use Modules\Users\Http\Requests\UsersRequest;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        return view('users::profile', [
            'userData' => $user,

            'title' => __('Profile'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'title' => __('Profile'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  ProfileUpdateRequest $request
     * @return Response
     */
    public function update(ProfileUpdateRequest $request)
    {
        $fields = (object) $request->all();

        $model = $request->user();
        $model->name = $fields->name;
        $model->login = $fields->login;
        $model->email = $fields->email;
        $model->document = $fields->document;
        $model->enabled = 'yes';

        if (isset($fields->password)) {
            $model->password = bcrypt($fields->password);
        }

        if ($model->save()) {
            $model->profile->update($fields->profile);
            $model->profile->address->update($fields->address);

            return redirect()
                ->route('profile.index', $request->reference)
                ->with('success', __('Content updated successfully.'));
        } else {
            return redirect()
                ->route('profile.index', $request->reference)
                ->with('fail', __('Content could not be updated.'))
                ->withInput(toArray($fields));
        }
    }

}
