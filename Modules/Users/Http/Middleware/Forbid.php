<?php

namespace Modules\Users\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Forbid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$type)
    {
        switch ($type):
            case (in_array('admin', $type)):
                $action = is_admin();
                break;

            case (in_array('representative', $type)):
                $action = is_representative();
                break;

            case (in_array('responsible', $type)):
                $action = is_responsible();
                break;

            case (in_array('partner', $type)):
                $action = is_client();
                break;

            default:
        endswitch;

        if ($action)
            abort(404, __('Page not found'));

        return $next($request);
    }
}
