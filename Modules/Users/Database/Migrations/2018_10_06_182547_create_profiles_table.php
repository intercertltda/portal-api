<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type_other_document', ['cnh', 'rg', 'nothing'])->default('nothing');
            $table->string('other_document')->nullable();
            $table->string('phone')->nullable();

            $table->string('genre', ['fem', 'masc', 'other'])->default('other');

            $table->morphs('profileable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
