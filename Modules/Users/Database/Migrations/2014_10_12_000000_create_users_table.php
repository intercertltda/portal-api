<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            /*
             * Define o Nome do usuário
             */
            $table->string('name');


            /*
             * Define o tipo de Usuário
             * user = Funcionário
             * client = Cliente
             */
            $table->string('type')->default('user');

            $table->string('email')->unique(); // email
            $table->string('login')->unique(); // login
            $table->string('document')->unique(); // CPF apenas números

            $table->enum('enabled', ['yes','no'])->default('no');
            $table->string('password');
            $table->timestamp('logged_at')->nullable();

            $table->uuid('reference')->unique();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
