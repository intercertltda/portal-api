<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Addresses\Entities\Address;
use Modules\Users\Entities\Profile;
use Modules\Users\Entities\User;

class UsersAdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $user = new User();
        $user->name = 'Felipe Rank';
        $user->login = 'felipe.rank';
        $user->email = 'contato.intercert@gmail.com';
        $user->document = '08240908969';
        $user->enabled = 'yes';
        $user->password = bcrypt('12345');

        $user->save();

        $profile = $user->profile()->create([
            'phone' => '8821015901'
        ]);


        $profile->address()->create([
            'street' => 'AV Ailton Gomes',
            'neighborhood' => 'Pirajá',
            'complement' => 'Loja',
            'number' => '2244',
            'code_postal' => '63034-012',
            'city' => 'Juazeiro do Norte',
            'state' => 'CE',
        ]);


        $user->assignRole('admin');
    }
}
