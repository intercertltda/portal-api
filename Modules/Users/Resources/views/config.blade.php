<div class="row">
    <div class="col-md-12">
        <h4 class="pull-left">Configurações</h4>
    </div>
</div>
<br>

<div class="row form-group">
    <div class="col-md-6">
        @component('system::components.select', [
            'select' => [
                'name' => 'collaborator_type',
                'title' => __('Select type'),
                'label' => __('Type of Collaborator'),
                'list' => [
                    'user' => 'Funcionário',
                    'client' => 'Cliente Colaborador',
                ],
                'old' => $collaboratorType ?? old('collaborator_type'),
                'required' => true
            ]
        ])
        @endcomponent
    </div>

    <div class="col-md-6">

        @component('system::components.select', [
            'select' => [
                'name' => 'user_type',
                'name-array' => 'user_type[]',
                'title' => __('Select type of user'),
                'label' => __('Type of Permission'),
                'list' => config('permissions.desc'),
                'old' => $userType ?? old('user_type'),
                'required' => true,
                'multiple' => true
            ]
        ])
        @endcomponent
    </div>
</div>