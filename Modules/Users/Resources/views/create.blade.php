@php
    if (request()->input('faker')) {
        $genres = [ 'fem', 'masc', 'other' ];
        $name = str_faker('name');
        $document = str_faker('cpf');
        $email = str_faker('email');
        $phone = str_faker('cellphoneNumber');
        $login = helpers()->nameToLogin($name);
        $genre = $genres[rand(0,2)];
    } else {
        $name = old('name');
        $document = old('document-cpf');
        $email = old('email');
        $phone = old('phone');
        $login = old('login');
        $genre = old('genre');
    }
@endphp
@extends('system::layouts.master')
@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        @if ($errors->any())
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                            'url' => route('users.store'),
                        ]) }}
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="pull-left">Colaborador</h4>
                                </div>
                            </div>
                            <br>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    {{ Form::label('name', 'Nome') }}
                                    {{ Form::text('name', $name, ['class' => 'form-control', 'id' => 'name', 'required']) }}
                                </div>


                                <div class="col-md-4">
                                    {{ Form::label('login', 'Login') }}
                                    {{ Form::text('login', $login, ['class' => 'form-control', 'id' => 'login', 'required']) }}
                                </div>

                                <div class="col-md-4">
                                    {{ Form::label('email', 'E-mail') }}
                                    {{ Form::email('email', $email, ['class' => 'form-control', 'id' => 'email', 'required']) }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    {{ Form::label('document', 'CPF') }}
                                    {{ Form::text('document', $document, ['class' => 'form-control document_cpf', 'id' => 'document', 'required']) }}
                                </div>

                                <div class="col-md-4">
                                    {{ Form::label('phone', 'Telefone') }}
                                    {{ Form::text('profile[phone]', $phone, ['class' => 'form-control cellphone', 'id' => 'phone', 'required']) }}
                                </div>

                                <div class="col-md-4">
                                    {{ Form::label('genre', 'Genêro') }} <br>
                                    <div class="radio-multiple">
                                        <label for="genre-fem">{{ Form::radio('profile[genre]', 'fem', $genre === 'fem', ['id' => 'genre-fem', 'required']) }} Feminino</label>
                                        <label for="genre-masc">{{ Form::radio('profile[genre]', 'masc', $genre === 'masc', ['id' => 'genre-masc', 'required']) }} Masculino</label>
                                        <label for="genre-other">{{ Form::radio('profile[genre]', 'other', $genre === 'other', ['id' => 'genre-other', 'required']) }} Outros</label>
                                    </div>
                                </div>

                            </div>

                            <hr class="col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="pull-left">Senha de Acesso</h4>
                                </div>
                            </div>
                            <br>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    {{ Form::label('password', 'Senha') }}
                                    {{ Form::password('password', ['class' => 'form-control password', 'id' => 'password', !isset($typeForm) ? 'required' : '']) }}
                                </div>

                                <div class="col-md-6">
                                    {{ Form::label('password_confirmation', 'Confirmação de Senha') }}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control password_confirmation', 'id' => 'password_confirmation', !isset($typeForm) ? 'required' : '']) }}
                                </div>
                            </div>
                            <hr class="col-12">

                            @component('addresses::form', [
                                'isArray' => true
                            ])
                            @endcomponent

                            @hasrole('admin')
                            @component('users::config')
                            @endcomponent
                            @endhasrole

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right">Cadastrar</button>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <style>
        .radio-multiple label {
            margin-bottom: 0;
            line-height: 40px;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/libs/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        $(".cellphone").inputmask({mask:'(99) [9] 9999-9999'});
        $(".document_cpf").inputmask({mask:'999.999.999-99'});
    </script>
@endpush
