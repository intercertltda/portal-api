@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @php
                            $thead['name'] = 'Nome';
                            $thead['document'] = 'Documento';
                            $thead['phone'] = 'Telefone';
                            $thead['email'] = 'E-mail';
                            $thead['actions'] = 'Ações';
                        @endphp
                        @component('system::components.table', [
                            'thead' => $thead
                        ])
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ helpers()->document($item->document, true)->mask() }}</td>
                                    <td>{{ helpers()->phone($item->profile->phone) }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('users.edit', $item->reference) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('users.show', $item->reference) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                        <a href="#" data-local="colaboradores" data-reference="{{ $item->reference }}" class="btn btn-danger js-confirm-action"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
