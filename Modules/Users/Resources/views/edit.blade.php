@php
    $genre = $userData->profile->genre ?? old('profile.genre');
    $name = $userData->name ?? old('name');
    $document = $userData->document ?? old('document-cpf');
    $email = $userData->email ?? old('email');
    $phone = $userData->profile->phone ?? old('profile.phone');
    $login = $userData->login ?? old('login');

@endphp
@extends('system::layouts.master')
@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                            'url' => route('users.update', $userData->reference),
                        ]) }}
                            {{ Form::hidden('__method', 'PUT') }}
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="pull-left">@lang('Data of Collaborator')</h4>
                                </div>
                            </div>
                            <br>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    {{ Form::label('name', 'Nome') }}
                                    {{ Form::text('name', $name, ['class' => 'form-control', 'id' => 'name', 'required']) }}
                                </div>


                                <div class="col-md-4">
                                    {{ Form::label('login', 'Login') }}
                                    {{ Form::text('login', $login, ['class' => 'form-control', 'id' => 'login', 'required']) }}
                                </div>

                                <div class="col-md-4">
                                    {{ Form::label('email', 'E-mail') }}
                                    {{ Form::email('email', $email, ['class' => 'form-control', 'id' => 'email', 'required']) }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    {{ Form::label('document', 'CPF') }}
                                    {{ Form::text('document', $document, ['class' => 'form-control document_cpf', 'id' => 'document', 'required']) }}
                                </div>

                                <div class="col-md-4">
                                    {{ Form::label('phone', 'Telefone') }}
                                    {{ Form::text('profile[phone]', $phone, ['class' => 'form-control cellphone', 'id' => 'phone', 'required']) }}
                                </div>

                                <div class="col-md-4">
                                    {{ Form::label('genre', 'Genêro') }} <br>
                                    <div class="radio-multiple">
                                        <label for="genre-fem">{{ Form::radio('profile[genre]', 'fem', $genre === 'fem', ['id' => 'genre-fem', 'required']) }} Feminino</label>
                                        <label for="genre-masc">{{ Form::radio('profile[genre]', 'masc', $genre === 'masc', ['id' => 'genre-masc', 'required']) }} Masculino</label>
                                        <label for="genre-other">{{ Form::radio('profile[genre]', 'other', $genre === 'other', ['id' => 'genre-other', 'required']) }} Outros</label>
                                    </div>
                                </div>

                            </div>

                            <hr class="col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="pull-left">Senha de Acesso</h4>
                                </div>
                            </div>
                            <br>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    {{ Form::label('password', 'Senha') }}
                                    {{ Form::password('password', ['class' => 'form-control password', 'id' => 'password']) }}
                                </div>

                                <div class="col-md-6">
                                    {{ Form::label('password_confirmation', 'Confirmação de Senha') }}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control password_confirmation', 'id' => 'password_confirmation']) }}
                                </div>
                            </div>
                            <hr class="col-12">

                            @component('addresses::form', [
                                'data' => $userData->profile->address,
                                'isArray' => true
                            ])
                            @endcomponent

                            @hasrole('admin')
                            @component('users::config', [
                                'collaboratorType' => $userData->type,
                                'userType' => $userData->getRoleNames()
                            ])
                            @endcomponent
                            @endhasrole

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right">@lang('Update')</button>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <style>
        .radio-multiple label {
            margin-bottom: 0;
            line-height: 40px;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/libs/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        $(".cellphone").inputmask({mask:'(99) [9] 9999-9999'});
        $(".document_cpf").inputmask({mask:'999.999.999-99'});
    </script>
@endpush
