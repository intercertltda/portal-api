<?php

namespace Modules\Users\Facades;


use Modules\Users\Entities\User;

class UsersActionsFacade
{
    /**
     * Create a new facade instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    static public function loggedAt(User $user)
    {
        $user->logged_at = now()->format('Y-m-d H:i:s');

        return $user->save() ?? false;
    }
}
