<?php

namespace Modules\Companies\Facades;


use Illuminate\Support\Collection;

class MyCompanies
{
    /**
     * @return mixed
     */
    static public function get()
    {
        return auth()->user()->companies;
    }

    /**
     * @return Collection
     */
    static public function steps() : Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->steps))
            {
                foreach ($company->steps as $step)
                {
                    $steps[] = $step;
                }
            }
        }

        return collect($steps ?? []);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static public function departments() : Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->departments))
            {
                foreach ($company->departments as $department)
                {
                    $departments[] = $department;
                }
            }
        }

        return collect($departments ?? []);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static public function collaborators() : Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->users))
            {
                foreach ($company->users as $collaborator)
                {
                    $collaborators[] = $collaborator;
                }
            }
        }

        return collect($collaborators ?? []);
    }
}
