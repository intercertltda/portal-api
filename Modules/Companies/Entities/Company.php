<?php

namespace Modules\Companies\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Reference;
use App\Traits\ModelHelpers;
use Modules\Addresses\Entities\Address;
use Modules\Departments\Entities\Department;
use Modules\Manager\Entities\Step;
use Modules\Users\Entities\User;

class Company extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    protected $table = 'companies';
    protected $fillable = [];

    protected $appends = [ 'users' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function representative() : HasOne
    {
        return $this->hasOne(User::class, 'id', 'representative_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function responsible() : BelongsToMany
    {
        return $this->belongsToMany(User::class, 'companies_responsible', 'company_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address() : MorphOne
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function departments() : HasMany
    {
        return $this->hasMany(Department::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function steps() : HasMany
    {
        return $this->hasMany(Step::class, 'company_id');
    }

    /**
     * @return mixed
     */
    public function getUsersAttribute()
    {
        $representative = $this->representative()->latest()->get();
        $responsible = $this->responsible()->latest()->get();

        return $representative->merge($responsible);
    }
}
