<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('representative_id')
                ->unsigned();

            $table->string('social_name');
            $table->string('document');
            $table->string('phone');
            $table->string('email');

            $table->foreign('representative_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
