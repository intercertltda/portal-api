@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @component('system::components.table', [
                            'thead' => [
                                'name' => 'Nome',
                                'responsible' => 'Responsável',
                                'document' => 'CNPJ',
                                'phone' => 'Telefone',
                                'email' => 'E-mail',
                                'actions' => 'Ações'
                            ]
                        ])
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ $item->social_name }}</td>
                                    <td>{{ $item->representative->name }}</td>
                                    <td>{{ helpers()->document($item->document)->mask() }}</td>
                                    <td>{{ helpers()->phone($item->phone) }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('companies.my.edit', $item->reference) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
