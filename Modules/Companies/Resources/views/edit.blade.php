@php
    $social_name = $companiesData->social_name ?? old('name');
    $document = $companiesData->document ?? old('document');
    $email = $companiesData->email ?? old('email');
    $phone = $companiesData->phone ?? old('phone');
    $representative = $companiesData->representative->reference ?? old('representative');
    $collaborators = $collaborators ?? old('collaborators');
@endphp
@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                            'url' => route('companies.update', $companiesData->reference),
                        ]) }}
                        {{ Form::hidden('__method', 'PUT') }}

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="pull-left">@lang('Data of Company')</h4>
                            </div>
                        </div>
                        <br>
                        <div class="row form-group">
                            <div class="col-md-4">
                                {{ Form::label('social_name', __('Social Name')) }}
                                {{ Form::text('social_name', $social_name, ['class' => 'form-control', 'id' => 'social_name', 'required']) }}
                            </div>
                            <div class="col-md-4">
                                @component('system::components.select', [
                                    'select' => [
                                        'name' => 'representative',
                                        'title' => 'Selecione um usuário',
                                        'label' => __('Legal Representative'),
                                        'list' => $representatives,
                                        'old' => $representative,
                                        'required' => true
                                    ]
                                ])
                                @endcomponent
                            </div>

                            <div class="col-md-4">
                                @component('system::components.select', [
                                    'select' => [
                                        'name' => 'collaborators',
                                        'name-array' => 'collaborators[]',
                                        'title' => 'Selecione um usuário',
                                        'label' => __('Collaborators'),
                                        'list' => $representatives,
                                        'old' => $collaborators,
                                        'multiple' => true
                                    ]
                                ])
                                @endcomponent
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                {{ Form::label('document', sprintf('%s (CNPJ)', __('Document'))) }}
                                {{ Form::text('document', $document, ['class' => 'form-control document_cnpj', 'id' => 'document', 'required']) }}
                            </div>

                            <div class="col-md-4">
                                {{ Form::label('email', __('E-mail')) }}
                                {{ Form::email('email', $email, ['class' => 'form-control', 'id' => 'email', 'required']) }}
                            </div>

                            <div class="col-md-4">
                                {{ Form::label('phone', __('Phone')) }}
                                {{ Form::text('phone', $phone, ['class' => 'form-control cellphone', 'id' => 'phone', 'required']) }}
                            </div>
                        </div>

                        <hr class="col-12">

                        @component('addresses::form', [
                            'isArray' => true,
                                'data' => $address
                        ])
                        @endcomponent

                        <div class="row form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">@lang('Update')</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/libs/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        $(".cellphone").inputmask({mask:'(99) [9] 9999-9999'});
        $(".document_cnpj").inputmask({mask:'99.999.999/9999-99'});
    </script>
@endpush
