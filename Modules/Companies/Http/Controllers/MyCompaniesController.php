<?php

namespace Modules\Companies\Http\Controllers;

use App\Classes\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Modules\Companies\Entities\Company as Model;
use Modules\Companies\Facades\MyCompanies;
use Modules\Companies\Http\Requests\CompaniesRequest;
use Modules\Companies\Transformers\CompanyResource as Resource;
use Modules\Users\Entities\User;

class MyCompaniesController extends Controller
{
    /**
     * MyCompaniesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['is:representative']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $companies = MyCompanies::get();

        return view('companies::my.index', [
            'items' => Resource::collection($companies),

            'title' => __('My Companies'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'title' => __('My Companies'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request)
    {
        $companies = Model::reference($request->reference);

        foreach($companies->responsible as $collaborator)
        {
            $collaborators[] = $collaborator->reference;
        }

        $users = DB::table('users')
            ->select(['reference', 'name'])
            ->get();

        return view('companies::my.edit', [
            'companiesData' => $companies,
            'collaborators' => $collaborators ?? [],
            'representatives' => Collection::setKey($users, 'reference', 'name') ?? [],
            'address' => $companies->address,

            'title' => __('Edit'),
            'subtitle' => null,
            'breadcrumb' => [
                [
                    'url' => route('dashboard.index'),
                    'title' => __('Dashboard'),
                    'current' => false,
                    'icon' => 'mdi mdi-view-dashboard'
                ],
                [
                    'url' => route('companies.my.index'),
                    'title' => __('Companies'),
                    'current' => false,
                    'icon' => null
                ],
                [
                    'title' => __('Edit'),
                    'current' => true,
                    'icon' => null
                ]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CompaniesRequest $request)
    {
        $fields = (object) $request->all();
        $model = Model::reference($request->reference);

        $model->social_name = $fields->social_name;
        $model->document = (string) helpers()->clean($fields->document);
        $model->phone = (string) helpers()->clean($fields->phone);
        $model->email = $fields->email;

        if ($model->save()) {
            $model->address()->update($fields->address);

            return redirect()
                ->route('companies.my.edit', $request->reference)
                ->with('success', __('Successfully updated content.'));
        } else {
            return redirect()
                ->route('companies.my.edit', $request->reference)
                ->with('fail', __('Content could not be updated.'));
        }
    }
}
