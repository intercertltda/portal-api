<?php

namespace Modules\Companies\Http\Requests;

use App\Rules\ValidateDocument;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Addresses\Http\Requests\AddressArray;

class CompaniesRequest extends FormRequest
{
    protected $requests = [
        AddressArray::class,
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $rules = [
            'social_name' => 'required',
            'document' => ['required', new ValidateDocument()],
            'phone' => 'required',
            'email' => 'required|email',
            'representative' => 'required'
        ];

        foreach($this->requests as $source)
        {
            $rules = array_merge($rules, (new $source)->rules());
        }

        return $rules;
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        $messages = [
            'social_name.required' => 'A Razão Social é obrigatória',
            'document.required' => 'O documento é obrigatório',
            'document.valid' => 'O documento precisa ser válido',
            'phone.required' => 'O telefone é obrigatório',
            'email.required' => 'O e-mail é obrigatório',
            'email.email' => 'O e-mail precisa ser válido',
            'representative.required' => 'O representante legal é obrigatório'
        ];

        foreach($this->requests as $source)
        {
            $messages = array_merge($messages, (new $source)->messages());
        }

        return $messages;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
