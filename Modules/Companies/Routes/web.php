<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\System\Facades\RouteFacade;

RouteFacade::group([
    'prefix' => 'empresas',
    'middleware' => ['auth'],
    'as' => 'companies.'
], 'CompaniesController');

Route::group([
    'prefix' => 'minhas-empresas',
    'as' => 'companies.my.',
    'middleware' => ['auth']
], function() {
    Route::get('', 'MyCompaniesController@index')->name('index');
    Route::get('{reference}/editar', 'MyCompaniesController@edit')->name('edit');
    Route::post('{reference}', 'MyCompaniesController@update')->name('update');
});