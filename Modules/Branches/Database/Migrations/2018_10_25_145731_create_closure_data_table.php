<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosureDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closure_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');

            $table->string('name');
            $table->string('document');
            $table->string('email');
            $table->string('phone');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('branch_id')
                ->references('id')
                ->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closure_data');
    }
}
