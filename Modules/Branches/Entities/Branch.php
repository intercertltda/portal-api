<?php

namespace Modules\Branches\Entities;

use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Addresses\Entities\Address;

class Branch extends Model
{
    use Reference,
        SoftDeletes;

    protected $table = 'branches';
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }
}
