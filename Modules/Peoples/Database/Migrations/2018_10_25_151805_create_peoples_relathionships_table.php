<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeoplesRelathionshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches_agents', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('people_id');
            $table->unsignedInteger('branch_id');

            $table->foreign('people_id')
                ->reference('id')
                ->on('agents');

            $table->foreign('branch_id')
                ->reference('id')
                ->on('branches');
        });

        Schema::create('branches_representatives', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('people_id');
            $table->unsignedInteger('branch_id');

            $table->foreign('people_id')
                ->reference('id')
                ->on('representatives');

            $table->foreign('branch_id')
                ->reference('id')
                ->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches_agents');
        Schema::dropIfExists('branches_representatives');
    }
}
