<?php

namespace Modules\Peoples\Entities;

use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Branches\Entities\Branch;
use Modules\Users\Entities\Profile;

class Representative extends Model
{
    use SoftDeletes,
        Reference;

    protected $table = 'representatives';
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function profile()
    {
        return $this->morphOne(Profile::class, 'profileable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function branch()
    {
        return $this->belongsToMany(Branch::class, 'branches_representatives', 'people_id', 'branch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function calls()
    {
        return $this->morphMany(Call::class, 'callable');
    }
}
