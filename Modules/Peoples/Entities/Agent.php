<?php

namespace Modules\Peoples\Entities;

use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Branches\Entities\Branch;
use Modules\Calls\Entities\Call;
use Modules\Users\Entities\Profile;

class Agent extends Model
{
    use Reference,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'profile_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function profile()
    {
      return $this->morphOne(Profile::class, 'profileable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function branch()
    {
        return $this->belongsToMany(Branch::class, 'branches_agents', 'people_id', 'branch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function calls()
    {
        return $this->morphMany(Call::class, 'callable');
    }

    // ??
    public function solicitation(){
      return $this->hasMany(Solicitation::class);
    }

}
