<?php

return [
    'name' => 'Permissions',

    'list' => [

        'lead' => [
            'type' => 'user',
            'perms' => [
                'user',
                'panel',


                'view process slide', // UC024
                'accept terms', // UC020, UC021
                'contact by', // UC019
                'register', // UC023
            ]
        ],

        'user' => [
            'type' => 'user',
            'perms' => [
                'user',
                'panel'
            ]
        ],

        'agent' => [
            'type' => 'client',
            'perms' => [
                'agent',
                'panel',


                'tracking code', // UC027, UC030
                'view feedback', // UC028
                'select equipments', // UC029
                'profile', // UC031
                'show process', // UC032
                'upload docs', // UC033
            ]
        ],

        'responsible' => [
            'type' => 'client',
            'perms' => [
                'responsible',
                'panel',

                'tracking code', // UC027, UC030
                'view feedback', // UC028
                'select equipments', // UC029
                'profile', // UC031
                'show process', // UC032
                'upload docs', // UC033
                'manage branches', // UC025
                'manage agents', // UC026
            ]
        ],

        'commercial' => [
            'type' => 'partner',
            'perms' => [
                'commercial',
                'panel',

                'leads', // UC043, UC042
                'manage leads', // UC043
                'contact leads', // UC042
                'go along leads', // UC042
            ]
        ],

        'marketing' => [
            'type' => 'partner',
            'perms' => [
                'marketing',
                'panel',

                'manage leads', // UC043
            ]
        ],

        'financial' => [
            'type' => 'partner',
            'perms' => [
                'financial',
                'panel',

                'collections', // UC036, UC035, UC034
                'generate collections', // UC036, UC035
                'approve collections', // UC034
            ]
        ],

        'support' => [
            'type' => 'partner',
            'perms' => [
                'support',
                'panel',

                'register info machines', // UC001
                'view agents', // UC002
                'schedule config', // UC003
                'config machine', // UC004
                'register feedback', // UC005
            ]
        ],

        'compliance' => [
            'type' => 'partner',
            'perms' => [
                'compliance',
                'panel',

                'view agents', // UC002
                'view documentation', // UC008, UC009
                'view photos ambient', // UC010
            ]
        ],

        'legal' => [
           'type' => 'partner',
           'perms' => [
               'legal',
               'panel',

               'models contracts', // UC039
               'activity pertinent', // UC040
           ]
        ],

        'instructor' => [
            'type' => 'partner',
            'perms' => [
                'instructor',
                'panel',

                'schedule trainings', // UC006
                'show trainings', // UC007
                'sign contracts', // UC038
                'analyse contracts', // UC0
            ]
        ],

        'directors' => [
            'type' => 'partner',
            'perms' => [
                'directors',
                'panel',

                'collaborator', // UC011
                'link to steps', // UC012
                'link to process', // UC013
                'process', // UC0
                'steps', // UC0
                'link steps', // UC016
                'order steps', // UC0
            ]
        ],

        'admin' => [
            'type' => 'admin',
            'perms' => [
                'directors',
                'admin',
                'panel',

                'collaborator', // UC011
                'link to steps', // UC012
                'link to process', // UC013
                'process', // UC0
                'steps', // UC0
                'link steps', // UC016
                'order steps', // UC0

                'schedule trainings', // UC006
                'show trainings', // UC007
                'sign contracts', // UC038
                'analyse contracts', // UC0

                'models contracts', // UC039
                'activity pertinent', // UC040

                'view agents', // UC002
                'view documentation', // UC008, UC009
                'view photos ambient', // UC010

                'register info machines', // UC001
                'view agents', // UC002
                'schedule config', // UC003
                'config machine', // UC004
                'register feedback', // UC005

                'collections', // UC036, UC035, UC034
                'generate collections', // UC036, UC035
                'approve collections', // UC034

                'leads', // UC042, UC043
                'manage leads', // UC043
                'contact leads', // UC042
                'go along leads', // UC042

                'tracking code', // UC027, UC030
                'view feedback', // UC028
                'select equipments', // UC029
                'profile', // UC031
                'show process', // UC032
                'upload docs', // UC033
                'manage branches', // UC025
                'manage agents', // UC026

                'tracking code', // UC027, UC030
                'view feedback', // UC028
                'select equipments', // UC029
                'profile', // UC031
                'show process', // UC032
                'upload docs', // UC033
            ]
        ]
    ],

    'desc' => [
        'admin' => 'Administrador do Sistema',
        'directors' => 'Administrador de Empresa',
        'instructor' => 'Instrutor',
        'legal' => 'Jurídico',
        'compliance' => 'Compliance',
        'support' => 'Suporte de TI',
        'financial' => 'Financeiro',
        'marketing' => 'Marketing',
        'commercial' => 'Commercial',
        'responsible' => 'Responsável pela ITS',
        'agent' => 'Agente de Registro',
        'user' => 'Usuário'
    ]
];
