<?php

namespace Modules\Permissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        /*
         * Permissions
         */
        $permissions = config('permissions.list');

        foreach ($permissions as $key => $perm)
        {
            foreach($perm['perms'] as $item)
            {
                $allPermissions[] = $item;
            }
        }

        $allPermissions = array_unique($allPermissions);

        foreach($allPermissions as $item)
        {
            if (!Permission::where('name', $item)->first()) {
                Permission::create(['name' => $item]);
            }
        }

        foreach($permissions as $name => $perms)
        {
            $role = Role::create(['name' => $name]);
            $role->givePermissionTo($perms['perms']);
        }
    }
}
