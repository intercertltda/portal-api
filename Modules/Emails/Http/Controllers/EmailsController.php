<?php

namespace Modules\Emails\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class EmailsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('emails::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('emails::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('emails::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @param string $reference
     * @return Response
     */
    public function edit(Request $request, string $reference)
    {
        return view('emails::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param string $reference
     * @return Response
     */
    public function update(Request $request, string $reference)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param string $reference
     * @return Response
     */
    public function destroy(Request $request, string $reference)
    {
    }
}
