<!-- 1 Column Text : BEGIN -->
<tr>
	<td bgcolor="#ffffff">
		<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
			@if(isset($title))
				<tr>
					<td style="padding: 40px 40px 0; border-bottom: 1px solid #ddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
						<h1 style="font-size: 20px;">{{ $title }}</h1>
					</td>
				</tr>
			@endif
			<tr>
				<td style="padding: {{ $margin or '40' }}px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
					{{ $slot }}
				</td>
			</tr>
			<tr>
				<td>
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 5px; background: #0099cc;"></td>
							<td style="padding: 5px; background: #006699;"></td>
							<td style="padding: 5px; background: #ff9933;"></td>
							{{-- <td style="padding: 5px; background: #999999;"></td> --}}
							{{-- <td style="padding: 5px; background: #fafafa;"></td> --}}
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<!-- 1 Column Text : END -->
