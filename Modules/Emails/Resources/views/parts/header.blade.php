<!-- Email Header : BEGIN -->
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
    <tr>
        <td style="padding: 30px 0 15px 0; font-size: 0; text-align: left">
            <a href="{{ config('app.website') }}">
            	<img src="{{ config('app.website') . '/images/site-logo.png' }}" width="200" height="50" border="0" style="height: auto; background: transparent; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
            </a>
        </td>
    </tr>
</table>
<!-- Email Header : END