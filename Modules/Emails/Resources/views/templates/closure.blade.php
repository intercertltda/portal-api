@extends('mail.layouts.main')

@section('content')
	@component('mail.components.text')
		{{-- @isset($title) --}}
			@slot('title')
				{{ $title or '' }}
			@endslot
		{{-- @endisset --}}

        <p style="font-size: 14px; margin-top: 0;">
            {!! $text or '' !!}
        </p>

        <div class="table-list-items">
            <!-- Button : BEGIN -->
            <table class="table-list" cellspacing="0" cellpadding="0" border="0" style="width: 100%; margin: auto">
                <thead style="padding-bottom: 10px;">
                    <tr>
                        <th style="border-bottom: 2px solid #eee !important; padding: 6px;" width="8%">#</th>
                        <th style="border-bottom: 2px solid #eee !important; padding: 6px;">Descrição</th>
                        <th style="border-bottom: 2px solid #eee !important; padding: 6px;" width="22%">Custo</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td style="border-bottom: 1px solid #efefef; padding: 12px 6px; line-height: 1.3;">{{ ($loop->index + 1) }}</td>
                        <td style="border-bottom: 1px solid #efefef; padding: 12px 6px; line-height: 1.3;">{{ $item['name'] }}</td>
                        <td style="border-bottom: 1px solid #efefef; padding: 12px 6px; line-height: 1.3;">{{ $item['type'] === 'discount' ? '- ' : '' }}R$ {{ str_value($item['value'], 'int-real') }}</td>
                    </tr>
                    @endforeach
                </tbody>

                <tfoot style="font-size: 18px;">
                    <tr>
                        <td style="padding: 12px 6px; line-height: 1.3;"><a href="{{ $linkToItems }}" title="Acessar Lista de Pedidos"><img src="{{ config('app.website') . '/images/icons/external-link-symbol.png' }}" alt="Acessar Lista de Pedidos" width="20px"></a></td>
                        <td style="padding: 12px 6px; line-height: 1.3; text-align: right;">Total:</td>
                        <td style="padding: 12px 6px; line-height: 1.3;"><strong>R$ {{ str_real($totals['value']) }}</strong></td>
                    </tr>
                </tfoot>
            </table>
            <!-- Button : END -->
        </div>

        @if(!$production)
            <p>Customer: {{ sprintf('%s - %s', $data['email'], $data['name']) }}</p>
        @endif

        @if(isset($button))
		<div style="padding: 25px;">
            <!-- Button : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                <tr>
                    <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                        <a href="{{ $button['url'] or '#' }}" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                            <span style="color:#ffffff;">{{ $button['title'] or 'Clique aqui' }}</span>
                        </a>
                    </td>
                </tr>
            </table>
            <!-- Button : END -->
        </div>
        @endif
	@endcomponent
@endsection
