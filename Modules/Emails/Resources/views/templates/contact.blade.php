@extends('mail.layouts.main')

@section('content')
	@component('mail.components.text')
		@isset($title)
			@slot('title')
				{{ $title or '' }}
			@endslot
		@endisset
		<p style="font-size: 14px;">
			<strong>Nome:</strong> {{ $contact->name }}<br>
			<strong>E-mail:</strong> {{ $contact->email }}<br>
			<strong>Assunto:</strong> {{ $contact->subject }}<br>
			<strong>Data de envio:</strong> {{ $date }}<br>
			<strong>Mensagem:</strong> <br>{{ $contact->message }}
		</p>
	@endcomponent
@endsection