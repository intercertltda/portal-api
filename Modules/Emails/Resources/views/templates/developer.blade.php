@extends('mail.layouts.main')

@section('content')
	@component('mail.components.text')
		@isset($title)
			@slot('title')
				{{ $title or '' }}
			@endslot
		@endisset
        
		<p style="font-size: 14px;">
			{{ $text or '' }}
		</p>

        <hr style="margin: 20px 0;">

        <code style="font-size: 12px;">
            {{ $__toString }}
        </code>

	@endcomponent
@endsection