@php
    if (request()->input('faker')) {
        $fakerLocation = str_faker_location();

        $neighborhood = str_faker('streetSuffix');
        $postalCode = str_faker('postcode');
        $complement = str_faker('secondaryAddress');
        $street = str_faker('streetName');
        $number = str_faker('buildingNumber');
        $state = $fakerLocation['state'];
        $city = $fakerLocation['city'];
    } else {
        $neighborhood = isset($data->neighborhood) ? $data->neighborhood : old('neighborhood');
        $postalCode = isset($data->code_postal) ? $data->code_postal : old('code_postal');
        $complement = isset($data->complement) ? $data->complement : old('complement');
        $street = isset($data->street) ? $data->street : old('street');
        $number = isset($data->number) ? $data->number : old('number');
        $state = isset($data->state) ? $data->state : old('state');
        $city = isset($data->city) ? $data->city : old('city');
    }
@endphp

<div class="row">
    <div class="col-md-12">
        <h4 class="pull-left">Endereço</h4>
    </div>
</div>
<br>
@slot('address')
    @isset($address)
        <div class="row">
            <div class="col-md-12">
                {{ $address }}
            </div>
        </div>
        <br>
    @endisset
@endslot
<div class="row form-group">
    <div class="col-md-3">
        {{ Form::label('code_postal', 'CEP') }}
        {{ Form::text(isset($isArray) && $isArray ? 'address[code_postal]' : 'code_postal', $postalCode, ['class' => 'form-control code_postal', 'id' => 'code_postal', 'required']) }}
    </div>

    <div class="col-md-3">
        {{ Form::label('street', 'Endereço') }}
        {{ Form::text(isset($isArray) && $isArray ? 'address[street]' : 'street', $street, ['class' => 'form-control street', 'id' => 'street', 'required']) }}
    </div>

    <div class="col-md-3">
        {{ Form::label('number', 'Número') }}
        {{ Form::text(isset($isArray) && $isArray ? 'address[number]' : 'number', $number, ['class' => 'form-control number', 'id' => 'number', 'required']) }}
    </div>

    <div class="col-md-3">
        {{ Form::label('complement', 'Complemento') }}
        {{ Form::text(isset($isArray) && $isArray ? 'address[complement]' : 'complement', $complement, ['class' => 'form-control complement', 'id' => 'complement']) }}
    </div>
</div>
<div class="row form-group">
    <div class="col-md-4">
        {{ Form::label('neighborhood', 'Bairro') }}
        {{ Form::text(isset($isArray) && $isArray ? 'address[neighborhood]' : 'neighborhood', $neighborhood, ['class' => 'form-control neighborhood', 'id' => 'neighborhood', 'required']) }}
    </div>

    <div class="col-md-4">
        @component('system::components.select', [
            'select' => [
                'name' => 'state',
                'name-array' => isset($isArray) && $isArray ? 'address[state]' : 'state',
                'title' => __('Select state'),
                'label' => __('State'),
                'list' => states()->list(),
                'old' => $state,
                'required' => true
            ]
        ])
        @endcomponent
    </div>

    <div class="col-md-4">
        @component('system::components.select', [
            'select' => [
                'name' => 'city',
                'name-array' => isset($isArray) && $isArray ? 'address[city]' : 'city',
                'title' => __('Select city'),
                'label' => __('City'),
                'list' => isset($city) ? states()->cities($state, false, true) : [],
                'old' => $city,
                'required' => true,
                'disabled' => isset($city) ? false : true
            ]
        ])
        @endcomponent
    </div>
</div>


@push('inline-scripts')
    <script type="text/javascript">
        (($) => {

            let states = @json(states()->cities()),
                selectCity = $('select.city'),
                selectState = $('select.state'),
                postalCode = $('.code_postal');

            postalCode.inputmask({mask:'99999-999'});

            postalCode.on('change', (e) => {
                const $this = $(e.currentTarget);
                getAddressByCEP($this.val());
            });

            selectState.selectpicker('render')
                .on('changed.bs.select', (e, clickedIndex, isSelected, previousValue) => {
                    let current = $(e.currentTarget).val();

                    applyCities(current);
                });

            const applyCities = (uf) => {
                selectCity.empty();

                $.each(states[uf], (i, item) => {
                    selectCity.append('<option value="' + item + '">' + item + '</option>');
                });

                selectCity.removeAttr('disabled')
                    .selectpicker('refresh');
            };

            //
            //
            // if (hasItemSelected) {
            //     applyCities(stateSelected);
            //
            //     setTimeout((e) => {
            //         selectState.val(stateSelected);
            //         selectCity.val(citySelected);
            //     }, 300);
            // }
            //
            const getAddressByCEP = (cep) =>
            {
                cep = cep.replace('-','');
                if(cep.length === 8){
                    $.ajax({
                        url: 'http://www.viacep.com.br/ws/' + cep + '/json/',
                        method: 'GET',
                        crossDomain: true,
                        dataType: 'jsonp',
                        success: (response) => {
                            if(typeof response !== 'undefined')
                                applyCities(response.uf);

                            $('input.street').val(response.logradouro);
                            $('input.neighborhood').val(response.bairro);

                            selectState.selectpicker('val', response.uf);
                            selectCity.selectpicker('val', response.localidade);
                            toastr.success('Endereço localizado com sucesso');


                            $('input.number').focus();
                        },
                        error: (err) => {
                            toastr.error('Não foi possível encontrar o endereço')
                        }
                    });
                }
            };
        })(jQuery)
    </script>
@endpush