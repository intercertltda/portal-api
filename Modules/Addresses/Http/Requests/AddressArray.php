<?php

namespace Modules\Addresses\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressArray extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'address.street'            => 'required',
            'address.neighborhood'      => 'required',
            'address.number'            => 'required',
            'address.code_postal'       => 'required',
            'address.city'              => 'required',
            'address.state'             => 'required'
        ];
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        return [
            'address.street.required'            => 'O endereço é obrigatório',
            'address.neighborhood.required'      => 'O bairro é obrigatório',
            'address.number.required'            => 'O número do local é obrigatório',
            'address.code_postal.required'       => 'O CEP do endereço é obrigatório',
            'address.city.required'              => 'A cidade é obrigatório',
            'address.state.required'             => 'O estado é obrigatório'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
