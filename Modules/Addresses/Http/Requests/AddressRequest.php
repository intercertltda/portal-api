<?php

namespace Modules\Addresses\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'street'            => 'required',
            'neighborhood'      => 'required',
            'number'            => 'required',
            'code_postal'       => 'required',
            'city'              => 'required',
            'state'             => 'required'
        ];
    }


    /**
     * @return array
     */
    public function messages() : array
    {
        return [
            'street.required'            => 'O endereço é obrigatório',
            'neighborhood.required'      => 'O bairro é obrigatório',
            'number.required'            => 'O número do local é obrigatório',
            'code_postal.required'       => 'O CEP do endereço é obrigatório',
            'city.required'              => 'A cidade é obrigatório',
            'state.required'             => 'O estado é obrigatório'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
