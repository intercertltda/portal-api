@extends('auth::layouts.master')

@section('content')
    <form class="form-horizontal m-t-20" id="loginform" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="row p-b-10{{ $errors->has('login') ? ' has-error' : '' }}">
            <div class="col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                    </div>
                    <input type="text" class="form-control form-control-lg" name="login" placeholder="Usuário" aria-label="Usuário" aria-describedby="basic-addon1" required value="{{ old('login') }}">
                </div>
                @if ($errors->has('login'))
                    <span class="help-block">
                <strong>{{ $errors->first('login') }}</strong>
              </span>
                @endif
            </div>
        </div>
        <div class="row p-b-30{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-key"></i></span>
                    </div>
                    <input type="password" class="form-control form-control-lg" name="password" placeholder="Senha" aria-label="Senha" aria-describedby="basic-addon1" required="">
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
                @endif
            </div>
        </div>
        <div class="row border-top border-secondary">
            <div class="col-12">
                <div class="form-group">
                    <div class="p-t-20">
                        <button role="button" class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Esqueceu sua senha?</button>
                        <button role="button" class="btn btn-success float-right" type="submit">Entrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop
