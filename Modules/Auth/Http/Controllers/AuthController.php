<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Facades\LoginFacade;
use Modules\Auth\Http\Requests\RequestLogin;
use Modules\Users\Facades\UsersActionsFacade;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login');
    }

    /**
     * Display a login resource.
     * @return Response
     */
    public function showLogin()
    {
        return view('auth::login', [
            'title' => __('Login')
        ]);
    }

    /**
     * Login Action
     * @param RequestLogin $request
     */
    public function postLogin(RequestLogin $request)
    {
        $login = $request->input('login');
        $pass = $request->input('password');
        $typeLogin = LoginFacade::check($login);

        $credentials[$typeLogin] = $login;
        $credentials['password'] = $pass;

        if (Auth::attempt($credentials)) {
            $user = \Auth::user();
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            if (session()->has('url.intended')) {
                return redirect(session()->get('url.intended'));
            } else {
                UsersActionsFacade::loggedAt(\Auth::user());
                return redirect()
                    ->route('dashboard.index');
            }
        } else {
            return back()
                ->withErrors([
                    'Autenticação falhou'
                ]);
        }
    }
}
