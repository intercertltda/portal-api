<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', function() {
    return redirect()
        ->to('entrar');
})->name('login');

//
//Route::get('register', function() {
//    return redirect()
//        ->to('cadastrar');
//})->name('register');
//
//Route::get('password/email', function() {
//    return redirect()
//        ->route('password.email');
//})->name('password.email');
//
//Route::get('password/request', function() {
//    return redirect()
//        ->route('password.request');
//})->name('password.request');
//
//Route::get('password/reset/{token}', function($token) {
//    return redirect()
//        ->route('password.reset', [
//            'token' => $token
//        ]);
//})->name('password.reset');

Route::group([
    'prefix' => 'entrar',
    'as' => '',
], function() {
    Route::get('', 'AuthController@showLogin')->name('login');
    Route::post('', 'AuthController@postLogin')->name('login.post');
});

Route::group([
    'prefix' => 'cadastrar',
    'as' => '',
], function() {
    Route::get('', 'AuthController@showRegister')->name('register');
    Route::post('', 'AuthController@postRegister')->name('register.post');
});

Route::group([
    'prefix' => 'resetar-senha',
    'as' => '',
], function() {
    Route::get('{token}', 'AuthController@showPasswordReset')->name('password.reset');
    Route::post('{token}', 'AuthController@postPasswordReset')->name('password.reset.post');
});

Route::group([
    'prefix' => 'esqueci-minha-senha',
    'as' => '',
], function() {
    Route::get('', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('', 'AuthController@postPasswordRequest')->name('password.request.post');
});

Route::get('sair', 'AuthController@logout')->name('logout');
