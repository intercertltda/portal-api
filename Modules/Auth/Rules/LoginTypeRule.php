<?php

namespace Modules\Auth\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Modules\Auth\Facades\LoginFacade;

class LoginTypeRule implements Rule
{
    /**
     * @var null|string
     */
    private $message;

    /**
     * Create a new rule instance.
     *
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        $this->message = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Check Exists
        return DB::table(config('users.models.user'))
            ->where(LoginFacade::check($value), $value)
            ->count() > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return isset($this->message) ? $this->message : 'O :attribute não existe em nossa base de dados';
    }
}
