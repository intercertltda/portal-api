<?php

namespace App\Traits;


trait ModelHelpers
{
    /**
     * @param $query
     * @param $reference
     * @return mixed
     */
    public static function scopeReference($query, $reference)
    {
        return $query
            ->where('reference', $reference)
            ->first();
    }
}