<?php

namespace App\Traits;

trait Reference
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->reference = uuid();
        });
    }
}