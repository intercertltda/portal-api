<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\System\Facades\ValidateFacade as Validate;

class ValidateDocument implements Rule
{
    /**
     * @var null|string
     */
    private $message;

    /**
     * Create a new rule instance.
     *
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        $this->message = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Validate::document($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return isset($this->message) ? $this->message : 'O campo :attribute precisa ser um documento válido';
    }
}
