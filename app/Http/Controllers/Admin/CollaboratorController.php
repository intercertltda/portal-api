<?php

namespace App\Http\Controllers\Admin;

use App\Collaborator;
use App\Department;
use App\Address;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Validator;

class CollaboratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $collaborators = Collaborator::all();
        return view('admin.collaborator.index', compact('collaborators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $departments = Department::all();
        return view('admin.collaborator.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['login'] != null){
          $validate = Validator::make($request->all(), [
            'login' => 'required|string|unique:users',
            'password' => 'required|string|min:8'
          ]);

          if($validate->fails()){
            return redirect('admin/colaborador/criar')->withInput()->withErros($validate);
          }
        }

        $validate = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'email' => 'required|email',
          'phone' => 'required',
          'department_id' => 'required',
          'responsibility' => 'required'
        ]);

        if($validate->fails()){
          return redirect('admin/colaborador/criar')->withInput()->withErros($validate);
        }

        try{
          $request['address_id'] = null;
          if($request['code_postal'] != null){
            $request['address_id'] = Address::create([
              'code_postal' => $request['code_postal'],
              'street' => $request['street'],
              'neighborhood' => $request['neighborhood'],
              'number' => $request['number'],
              'state' => $request['state'],
              'city' => $request['city']
            ])->id;

          }
          $profile = Profile::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'address_id' => $request['address_id']
          ]);
          if($request['department'] != null){
            $request['department_id'] = Department::create([
              'name' => $request['department']
            ])->id;
          }
          Collaborator::create([
            'responsibility' => $request['responsibility'],
            'department_id' => $request['department_id'],
            'profile_id' => $profile->id,
          ]);
          return redirect('admin/colaborador')->with(['status'=>'success', 'msg'=>'Colaborador cadastrado com sucesso']);
        }catch(\Exception $e){
          return redirect('admin/colaborador/criar')->withInput()->with(['status'=>'error', 'msg'=>'Falha ao tentar cadastrar novo colaborador']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Collaborator  $collaborator
     * @return \Illuminate\Http\Response
     */
    public function show(Collaborator $collaborator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collaborator  $collaborator
     * @return \Illuminate\Http\Response
     */
    public function edit(Collaborator $collaborator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Collaborator  $collaborator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collaborator $collaborator)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collaborator  $collaborator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collaborator $collaborator)
    {
        //
    }
}
