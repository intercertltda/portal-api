<?php

namespace App\Http\Controllers\Admin;

use App\Call;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $calls = Call::all();
        return view('admin.call.index', compact('calls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\call  $call
     * @return \Illuminate\Http\Response
     */
    public function show(call $call)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\call  $call
     * @return \Illuminate\Http\Response
     */
    public function edit(call $call)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\call  $call
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, call $call)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\call  $call
     * @return \Illuminate\Http\Response
     */
    public function destroy(call $call)
    {
        //
    }
}
