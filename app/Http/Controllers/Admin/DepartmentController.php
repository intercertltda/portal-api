<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Collaborator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $departments = Department::all();
        return view('admin.department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $collaborators = Collaborator::all();
        return view('admin.department.create', compact('collaborators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valide = Validator::make($request['name'], [
          'name' => 'required|string|unique:departments|max:255'
        ]);

        if($valide->fails()){
          return redirect()->back()->withErrors($valide->fails())->withInput();
        }

        $leader = Collaborator::find($request['leader']);
        if(!$leader){
          $request['leader'] = null;
        }

        Department::create([
          'name' => $request['name'],
          'leader' => $request['leader'],
          'description' => $request['description'],
        ]);

        return redirect('admin/departamento')->with(['status'=>'success', 'msg'=>'Departamento cadastrado com sucesso!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        //
    }
}
