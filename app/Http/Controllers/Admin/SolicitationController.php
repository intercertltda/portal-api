<?php

namespace App\Http\Controllers\Admin;

use App\Solicitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SolicitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $solicitations = Solicitation::all();
        return view('admin.solicitation.index', compact('solicitations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.solicitation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function show(Solicitation $solicitation)
    {
        //
        $solicitation = Solicitation::find($solicitation);
        return view('admin.solicitation.show', compact('solicitation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function edit(Solicitation $solicitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solicitation $solicitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solicitation $solicitation)
    {
        //
    }
}
