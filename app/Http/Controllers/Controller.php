<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @param $model
     * @param $route
     * @return Response
     */
    public function delete_method(Request $request, $model, $route)
    {
        if ($model->delete()) {
            if ($request->ajax()) {
                return response()
                    ->json([
                        'message' => __('Item moved to trash.')
                    ], 200);
            } else {
                return redirect()
                    ->route($route)
                    ->with('success', __('Item moved to trash.'));
            }
        } else {
            if ($request->ajax()) {
                return response()
                    ->json([
                        'message' => __('This item could not be moved to the trash.')
                    ], 402);
            } else {
                return redirect()
                    ->route($route)
                    ->with('error', __('This item could not be moved to the trash.'));
            }
        }
    }

    /**
     * @param Request $request
     * @param array $params
     * @param string $route
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function message(Request $request, array $params, string $route)
    {
        if ($request->ajax()) {
            return response()
                ->json([
                    'type' => $params['type'] ?? 'info',
                    'message' => $params['message']
                ], $params['status'] ?? 400);
        } else {
            return redirect()
                ->route($route)
                ->with($params['type'] ?? 'info', $params['message']);
        }
    }
}
