<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return mixed
     */
    public function boot(Request $request)
    {
        Route::resourceVerbs([
            'create' => 'criar',
            'edit' => 'editar',
        ]);

        Schema::defaultStringLength(191);


        view()->composer('*', function($view) use ($request) {
            $params['user'] = auth()->user();


            if (isset($params))
                foreach($params as $key => $data)
                    $view->with($key, $data);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
