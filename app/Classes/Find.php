<?php

namespace App\Classes;


use Illuminate\Support\Facades\DB;

class Find
{
    /**
     * @param string $table
     * @param array $where
     * @param string $field
     * @return mixed
     */
    static public function db(string $table, array $where, string $field = 'id')
    {
        return DB::table($table)
            ->where($where)
            ->first()
            ->{$field} ?? null;
    }
}