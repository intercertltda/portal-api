<?php

namespace App\Classes;


class Collection
{
    /**
     * @param $items
     * @param string $key
     * @param string $value
     * @return array
     */
    static public function setKey($items, string $key, string $value) : array
    {
        foreach ($items as $item)
            $list[$item->{$key}] = $item->{$value};

        return $list;
    }

    /**
     * @param $items
     * @param string $column
     * @return array
     */
    static public function getColumn($items, string $column) : array
    {
        return $items->map(function ($item) use ($column) {
            return $item->{$column};
        })->toArray();
    }
}