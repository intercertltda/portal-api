<?php
/**
 * File for check user
 */


if (!function_exists('is_admin'))
{
    /**
     * Check user if is admin
     * @return mixed
     */
    function is_admin()
    {
        $user = request()->user();
        return $user->hasRole('admin');
    }
}

if (!function_exists('is_director'))
{
    /**
     * @return mixed
     */
    function is_director()
    {
        $user = request()->user();
        return $user->hasRole('directors');
    }
}

if (!function_exists('has_company'))
{
    /**
     * @return bool
     */
    function has_company()
    {
        return request()->user()->companies->count() > 0;
    }
}

if (!function_exists('is_responsible'))
{
    /**
     * @return bool
     */
    function is_responsible()
    {
        return request()->user()->responsible()->count() > 0;
    }
}

if (!function_exists('is_representative'))
{
    /**
     * @return bool
     */
    function is_representative()
    {
        return request()->user()->representative()->count() > 0;
    }
}

if (!function_exists('is_client'))
{
    /**
     * @return bool
     */
    function is_client()
    {
        return is_responsible() or is_representative();
    }
}

if (!function_exists('is_partner'))
{
    /**
     * @return bool
     */
    function is_partner()
    {
        return request()->user()->type === 'user';
    }
}

if (!function_exists('has_role'))
{
    /**
     * @param $role
     * @return bool
     */
    function has_role($role)
    {
        $roles = config('permissions.list');
        $user = request()->user();

        if (is_array($role)) return $user->hasAnyRole($role);
        if (!isset($roles[$role])) return false;

        return $user->hasRole($role);
    }
}

if (!function_exists('has_perms'))
{
    /**
     * @param $perms
     * @return mixed
     */
    function has_perms($perms)
    {
        $user = request()->user();

        if (is_array($perms)) return $user->hasAnyPermission($perms);

        return $user->hasPermissionTo($perms);
    }
}

if (!function_exists('is_check'))
{
    function is_check($param, string $type)
    {
        switch ($type) {
            case 'role':
                return has_role($param);
                break;
            case 'permission':
                return has_perms($param);
                break;
            case 'type':
                switch ($param) {
                    case 'representative':
                        return is_representative();
                        break;
                    case 'responsible':
                        return is_responsible();
                        break;
                    case 'partner':
                        return is_partner();
                        break;
                    case 'client':
                        return is_client();
                        break;
                    default:
                        return is_admin();
                        break;
                }
                break;
            default:
        }

        return false;
    }
}