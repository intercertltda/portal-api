<?php
/**
 * Function of helpers for this system
 */

use Faker\Factory;
use Modules\System\Facades\HelpersFacade;
use Modules\System\Facades\StatesFacade;
use Webpatser\Uuid\Uuid;

if (!function_exists('str_faker'))
{
    /**
     * @param string $type
     * @return mixed
     */
    function str_faker(string $type = 'name', string $locale = 'pt_BR')
    {
        $faker = Factory::create($locale);
        return $faker->{$type};
    }
}

if (!function_exists('str_faker_location'))
{
    function str_faker_location()
    {
        $states = toArray(states()->estados);
        $total = count($states);
        $rand = rand(0, ($total - 1));
        $get = $states[$rand];

        $totalCities = count($get['cidades']);
        $randCities = rand(0, ($totalCities - 1));
        $getCity = $get['cidades'][$randCities];

        return [
            'state' => $get['sigla'],
            'city'  => $getCity
        ];
    }
}

if (!function_exists('uuid'))
{
    /**
     * @return string
     * @throws Exception
     */
    function uuid()
    {
        return Uuid::generate(5, rand(0, 1000) . '-' . date('Y-m-d H:i:s') . '-' . rand(0, 10000), Uuid::NS_DNS)->string;
    }
}

if (!function_exists('app_title'))
{
    /**
     * @param string|null $title
     * @param string|null $subtitle
     * @param string $separator
     * @return \Illuminate\Config\Repository|mixed|string
     */
    function app_title(string $title = null, string $subtitle = null, string $separator = '|')
    {
        $name = config('system.name');

        if (!isset($title))
            return $name;

        $string = sprintf('%s %s', $title, $separator);

        if (isset($subtitle))
            $string .= sprintf(' %s %s', $subtitle, $separator);

        return sprintf('%s %s', $string, $name);
    }
}

if (!function_exists('get_route'))
{
    /**
     * @param string $route
     * @param string $default
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function get_route(string $route, string $default = '/')
    {
        if (\Route::has($route)) {
            return route($route);
        } else {
            return url($default);
        }
    }
}

if (!function_exists('in_route'))
{
    /**
     * @param string $check
     * @return bool
     */
    function in_route(string $check)
    {
        return str_contains(\Route::currentRouteName(), $check);
    }
}

if (!function_exists('states'))
{
    /**
     * @return StatesFacade
     */
    function states() : StatesFacade
    {
        return new StatesFacade(storage_path('app/public/states-cities.json'));
    }
}

if (!function_exists('caching'))
{
    /**
     * @return \Modules\System\Facades\CachingFacade
     */
    function caching()
    {
        return (new \Modules\System\Facades\CachingFacade());
    }
}

if (!function_exists('collection'))
{
    function collection($data)
    {
//        return (new \Modules\System\Facades\Collection($data));

        return (new \Modules\System\Facades\CollectionFacade($data));
    }
}

if (!function_exists('array_find'))
{
    /**
     * @param $haystack
     * @param $needle
     * @return array
     */
    function array_find($haystack, $needle, string $inColumn = null)
    {
        // 1 - arr principal
        foreach ($haystack as $a => $b)
            if (is_collect($b)):
                // 2 - arr com as keys nomeadas
                foreach ($b as $c => $d):
                    if (is_collect($d)):
                        // 3 - arr com dados e keys nomeadas ou arr com coleção
                        if (isset($inColumn)) {
                            $ex = explode('.', $inColumn);

                            if (str_contains($ex[0], ',')) {
                                $exSub = explode(',', $ex[0]);

                                $keyPrimary = in_array($c, $exSub);
                            } else {
                                $keyPrimary = $ex[0] === $c;
                            }


                            $keySecondary = $ex[1];

                            foreach($d as $e => $f):
                                if (is_collect($f)):
                                    // 4 - arr de coleção

                                    foreach($f as $g => $h):
                                        if ($keyPrimary && $keySecondary === $g && is_string($h) && stripos($h, $needle) !== false)
                                            $items[$a] = $b;
                                    endforeach;
                                else:
                                    if ($keyPrimary && $keySecondary === $e && stripos($f, $needle) !== false):
                                        $items[$a]= $b;
                                    endif;
                                endif;
                            endforeach;
                        } else {
                            foreach($d as $e => $f):
                                if (is_collect($f)):
                                    // 4 - arr de coleção
                                    foreach($f as $g => $h):
                                        if (is_string($h) && stripos($h, $needle) !== false)
                                            $items[$a] = $b;
                                    endforeach;
                                else:
                                    if (stripos($f, $needle) !== false):
                                        $items[$a]= $b;
                                    endif;
                                endif;
                            endforeach;
                        }
                    else:
                        if (stripos($d, $needle) !== false):
                            $items[$a]= $b;
                        endif;
                    endif;
                endforeach;
            else:
                if (is_string($b) && stripos($b, $needle) !== false)
                    $items[$a] = $b;
            endif;

        return $items ?? [];
    }
}

if (!function_exists('is_collect'))
{
    function is_collect($collection)
    {
        return is_object($collection) or is_array($collection);
    }
}

if (!function_exists('is_string_int'))
{
    function is_string_int($str)
    {
        return is_string($str) or is_int($str);
    }
}

if (!function_exists('array_search_recursive'))
{
    /**
     * @param $needle
     * @param array $array
     * @return bool
     */
    function array_search_recursive($needle, array $array)
    {
        foreach ($array as $a => $b)
        {
            if (is_string_int($b)) {
                if (str_search($b, $needle)) return true;
            } elseif (is_collect($b)) {
                return array_search_recursive($needle, $b);
            }
        }

        return false;
    }
}

if (!function_exists('simple_search'))
{
    /**
     * @param $array
     * @param $needle
     * @return bool
     */
    function simple_search($array, $needle) : bool
    {
        if (is_string_int($array)) {
            if (str_search($array, $needle)) return true;
        } elseif (is_collect($array)) {
            foreach ($array as $c => $d)
            {
                if (is_string_int($d)) {
                    if (str_search($d, $needle)) return true;
                } elseif (is_collect($d)) {
                    foreach ($d as $e => $f)
                    {
                        if (is_string_int($f) && str_search($f, $needle)) return true;
                    }
                }
            }
        }

        return false;
    }
}

if (!function_exists('str_search'))
{
    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    function str_search($haystack, $needle)
    {
        return stripos($haystack, $needle) !== false;
    }
}

if (!function_exists('toArray'))
{
    /**
     * @param $d
     * @return array
     */
    function toArray($collection)
    {
        if (is_object($collection))
            $collection = get_object_vars($collection);

        return is_array($collection) ? array_map(__FUNCTION__, $collection) : $collection;
    }
}

if (!function_exists('toObject'))
{
    /**
     * @param $array
     * @return object
     */
    function toObject($array)
    {
        return is_array($array) ? (object) array_map(__FUNCTION__, $array) : $array;
    }
}

if (!function_exists('url_replace'))
{
    /**
     * @param string|null $removal
     * @param array|null $appends
     * @return string
     */
    function url_replace(string $removal = null, array $appends = null, string $url = null) : string
    {
        if (isset($url)) {
            $queryString = str_replace([request()->url(), '?'], '', request()->fullUrl());
        } else {
            $queryString = request()->getQueryString();
        }

        $items = explode('&', $queryString);

        if (isset($items))
            foreach($items as $item)
            {
                $queryEx = explode('=', $item);
                if (!empty($item) && isset($queryEx))
                    $queries[$queryEx[0]] = $queryEx[1];
            }

        if (isset($queries)):
            if (isset($queries[ $removal ]))
                unset($queries[ $removal ]);

            if (isset($appends))
                $queries = array_merge($queries, $appends);

            $queries = http_build_query($queries);
            $uri = sprintf('%s?%s', request()->url(), $queries);
        endif;

        return $uri ?? request()->url();
    }
}

if (!function_exists('helpers'))
{
    /**
     * @return HelpersFacade
     */
    function helpers() : HelpersFacade
    {
        return (new HelpersFacade);
    }
}