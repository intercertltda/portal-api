/*jshint esversion: 6 */
(($) => {
    'use strict';

    const ConfirmAction = $('.js-confirm-action');

    ConfirmAction.on('click', (e) => {
        const $this = $(e.currentTarget);

        if (confirm('Você deseja realmente deletar esse item?')) {
            $.ajax({
                url: $this.data('local') + '/' + $this.data('reference'),
                method: 'DELETE',
                data: {
                    test: 'test',
                },
                success: (response) => {
                    toastr.success(response.message);

                    $this.parents('tr').hide();

                    setTimeout((e) => {
                        $this.parents('tr').remove();
                    }, 700);
                },
                error: (err) => {
                    toastr.error(err.message);
                }
            });
        }

        return false;
    });


})(jQuery);


