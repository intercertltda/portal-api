<!DOCTYPE html>
<html dir="ltr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/logo.svg') }}">
  <title>Login - Intercet</title>
  <!-- Custom CSS -->
  <link href="{{ asset('assets/css/style.min.css') }}" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <div class="main-wrapper">
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
      <div class="auth-box bg-dark border-top border-secondary">
        <div class="text-center p-t-20 p-b-20">
          <span class="db"><img src="{{ asset('assets/images/logo.svg')}}" alt="logo" style="width:183px;height:50px;"></span>
        </div>
        <!-- Form -->
        <form class="form-horizontal m-t-20" id="loginform" method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="row p-b-10{{ $errors->has('login') ? ' has-error' : '' }}">
            <div class="col-12">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                </div>
                <input type="text" class="form-control form-control-lg" name="login" placeholder="Usuário" aria-label="Usuário" aria-describedby="basic-addon1" required value="{{ old('login') }}">
              </div>
              @if ($errors->has('login'))
              <span class="help-block">
                <strong>{{ $errors->first('login') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="row p-b-30{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="col-12">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-key"></i></span>
                </div>
                <input type="password" class="form-control form-control-lg" name="password" placeholder="Senha" aria-label="Senha" aria-describedby="basic-addon1" required="">
              </div>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="row border-top border-secondary">
            <div class="col-12">
              <div class="form-group">
                <div class="p-t-20">
                  <button role="button" class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Esqueceu sua senha?</button>
                  <button role="button" class="btn btn-success float-right" type="submit">Entrar</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- All Required js -->
  <!-- ============================================================== -->
  <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap tether Core JavaScript -->
  <script src="{{ asset('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>

</html>
