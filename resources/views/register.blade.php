<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('assets/libs/wizard/fonts/material-icon/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('assets/libs/wizard/css/style.css')}}">
    <style media="screen">
      input{
        border-bottom:none !impotant;
      }
    </style>
</head>
<body>

    <div class="main">
        <center><a href="http://www.intercet.com.br"><img src="{{ asset('assets/images/logo.svg') }}" style="width:183px;height:50px;"></a></center>
        <br>
        <div class="container">
            <form method="POST" id="signup-form" class="signup-form" action="#">
                <div>
                    <h3></h3>
                    <fieldset>
                        <input type="text" name="recommend" id="recommend" class="accept-mask" />
                        <label for="recommend" class="form-label">Tem alguma indicação interna/externa?</label>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <input type="text" name="is_client" id="is_client" class="accept-mask" />
                        <label for="is_client" class="form-label">Já comprou algum produto Intercet?</label>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <input type="text" name="name" id="name" />
                        <label for="name" class="form-label">Informe seu nome</label>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <input type="tel" name="phone" id="phone" class="phone-mask" />
                        <label for="phone" class="form-label">Informe seu telefone</label>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>

    <!-- JS -->
    <script src="{{ asset('assets/libs/wizard/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/libs/wizard/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets/libs/wizard/vendor/jquery-validation/dist/additional-methods.min.js')}}"></script>
    <script src="{{ asset('assets/libs/wizard/vendor/jquery-steps/jquery.steps.min.js')}}"></script>
    <script src="{{ asset('assets/libs/wizard/js/main.js')}}"></script>
    <script src="{{ asset('assets/libs/inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
    <script type="text/javascript">
      $(".phone-mask").inputmask({"mask": "(99) 9 9999-9999"});

    </script>
</body>
</html>
