<?php

return [
    'business' => [
        'name' => 'AR InterCert',
        'address' => 'Av. Ailton Gomes de Alencar, 2244, Pirajá - Juazeiro do Norte - Ce',
        'phone' => '8821015901',
        'email' => 'contato@intercert.com.br'
    ],

    'url' => [
        'contact' => 'https://intercert.com.br/contato',
        'support' => 'https://intercert.com.br/contato',
        'faq' => 'https://intercert.com.br/contato',
    ],


    'cache' => [
        // minutes * hours = total in minutes
        'small' => 60,
        'list' => 60 * 3,
        'long' => (60 * 24) * 7, // 7 days,
        'daily' => 60 * 24,
        'medium' => 60 * 3,
    ]
];