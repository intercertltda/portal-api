#!/usr/bin/env bash

php artisan module:migrate Addresses
php artisan module:migrate Auth
php artisan module:migrate Permissions --seed
php artisan module:migrate Users --seed
php artisan module:migrate Departments --seed
php artisan module:migrate Manager --seed
